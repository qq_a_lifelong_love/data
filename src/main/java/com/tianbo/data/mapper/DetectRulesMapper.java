package com.tianbo.data.mapper;

import com.tianbo.data.entity.DetectRules;
import com.tianbo.data.vo.EquTypeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/24
 */
@Mapper
public interface DetectRulesMapper {

    /**
     * 诊断规则的插入
     *
     * @param detectRules 诊断规则的数据线信息
     * @return 受影响的行数
     */
    Integer insertDetectRules(DetectRules detectRules);

    /**
     * 删除当前的诊断规则
     *
     * @param detectRulesId  诊断规则的id
     * @return 受影响的行数
     */
    Integer deleteDetectRulesById(Long detectRulesId);


    /**
     * 根据detectRulesId修改诊断规则的信息
     *
     * @param detectRules  诊断队则的对象
     * @return 受影响的行数
     */
    Integer updateDetectRules(DetectRules detectRules);

    /**
     * 只修改图片
     *
     * @param detectRules 诊断规则对象
     * @return
     */
    Integer updateImageById(DetectRules detectRules);

    /**
     * 查詢当前诊断规则信息
     *
     * @param detectRulesId  诊断信息的id
     * @return 根据id查询当前的诊断规则信息
     */
    DetectRules findDetectRulesById(Long detectRulesId);

    /**
     * 查詢当前诊断规则信息
     *
     * @param detectRulesId  诊断信息的id
     * @return 根据id查询当前的诊断规则信息
     */
    DetectRules getOne1(Long detectRulesId);


    /**
     * 根据设备类型id查询诊断规则
     *
     * @param equTypeId 设备类型id
     * @return
     */
    List<DetectRules> findDetectRulesByEquTypeId(Long equTypeId);


    /**
     * 获取最大的rulecode
     * @return
     */
    String getRuleCode(@Param("belongType") String belongType);


    /** =======================================================================
     * 根据排序查找 equType
     * @param belongType
     * @return
     */
    /**
     * 根据排序查找equType
     * @param belongType 所属类型（0-1-2：变电-输电 -配电，其它无效）
     * @return
     */
    List<EquTypeVo> selectTypeList(@Param("belongType") String belongType);

    /**
     * 通过查询诊断规则的详情
     *
     * @param belongType  所属类型
     * @param equTypeId 设备类型id
     * @return
     */
    List<DetectRules> selectTypeAndTypeIdList(@Param("belongType")String belongType, @Param("equTypeId")Long equTypeId);


    List<DetectRules> selectIdArray(@Param("belongType")String belongType, @Param("equTypeId") Long equTypeId);

    /**
     * 逻辑恢复
     * @param list
     * @return
     */
    int recover(@Param("list") List<Long> list, @Param("time") LocalDateTime time);

    /**
     * 根据变电-配电-输电查询数据
     * @param belongType
     * @return
     */
    List<DetectRules> selectByBelongType(@Param("belongType") String belongType, @Param("equTypeName") String equTypeName);
}
