package com.tianbo.data.service.impl;

import com.tianbo.data.entity.EquType;
import com.tianbo.data.mapper.EquTypeMapper;
import com.tianbo.data.service.EquTypeService;
import com.tianbo.data.service.ex.DeleteException;
import com.tianbo.data.service.ex.EquTypeNotFountException;
import com.tianbo.data.service.ex.InsertException;
import com.tianbo.data.service.ex.UpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/26
 */
@Service
public class EquTypeServiceImpl implements EquTypeService {

    @Autowired
    private EquTypeMapper equTypeMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addEquType(EquType equType) {
        EquType equTypes = equTypeMapper.findByEquTypeId(equType.getEquTypeId());
        if (equTypes != null) {
            throw new InsertException("当前设备类型已经存在,无法进行添加");
        }

        //        补全日志信息
//        equType.setCreator(5555555l);
//        equType.setUpdater(5555555l);
        equType.setCreateTime(new Date());
        equType.setUpdateTime(new Date());
        equType.setVersion(0);
        equType.setSorted(0);
        equType.setDeleted(0);
        Integer integer = equTypeMapper.insertEquType(equType);
        if (integer == 0) {
            throw new InsertException("添加时产生未知的异常");
        }


    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteEquType(Long equTypeId) {

        EquType equType = equTypeMapper.findByEquTypeId(equTypeId);
        if (equType == null) {
            throw new EquTypeNotFountException("当前数据信息查到不到");
        }
//        才去的数软删除，内部用的是更新deleted字段（0-1）
        equTypeMapper.deleteByFormulaId(equTypeId);

    /*    Integer integer = equTypeMapper.deleteEquType(equTypeId);
        if (integer==0){
            throw new DeleteException("删除时产生未知的异常");
        }*/

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateEquType(EquType equType) {

        EquType equTypes = equTypeMapper.findByEquTypeId(equType.getEquTypeId());
        if (equTypes == null) {
            throw new EquTypeNotFountException("当前数据信息查到不到产生的异常");
        }
        //        补全日志信息
//        equType.setUpdater(5555555l);
        equType.setUpdateTime(new Date());
        equType.setVersion(equType.getVersion());
        Integer integer = equTypeMapper.updateEquType(equType);
        if (integer == 0) {
            throw new UpdateException("更新数据信息时产生未知的异常");
        }
    }

    @Override
    public EquType getByEquTypeId(Long equTypeId) {
        EquType equType = equTypeMapper.findByEquTypeId(equTypeId);
        if (equType == null) {
            throw new EquTypeNotFountException("没有查询到设备信息");
        }
        return equType;
    }
}
