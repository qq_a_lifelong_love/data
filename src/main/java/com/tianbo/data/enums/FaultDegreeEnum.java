package com.tianbo.data.enums;

import com.google.common.collect.Sets;
import com.tianbo.data.utils.StringUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/14
 */
public enum FaultDegreeEnum {

//    缺陷等级: 1-危急, 2-严重, 3-一般, 4-正常, 99-未知;

    //危急
    DANGER("1", "危急"),
    //严重
    SERIOUS("2", "严重"),
    //一般
    GENERAL("3", "一般"),
    //正常
    NORMAL("4", "正常");
    //未知
//    UNKNOWN("99", "未知");

    private String degree;
    private String name;


    FaultDegreeEnum(String degree, String name) {
        this.degree = degree;
        this.name = name;
    }

    public static final String DEGREE = "degree";

    public String getDegree() {
        return degree;
    }

    public String getName() {
        return name;
    }


    public static FaultDegreeEnum getEnum(String degree, String name) {
        if (StringUtils.isNotEmpty(degree)) {
            for (FaultDegreeEnum e : FaultDegreeEnum.values()) {
                if (e.getDegree().equals(degree)) {
                    return e;
                }
            }
        }

        if (StringUtils.isNotEmpty(name)) {
            for (FaultDegreeEnum e : FaultDegreeEnum.values()) {
                if (e.getName().equals(name.trim())) {
                    return e;
                }
            }
        }

        return null;
    }

    public Set<String> getDegreeSet(){
        HashSet<String> hashSet = Sets.newHashSet();
        for (FaultDegreeEnum fde : FaultDegreeEnum.values()){
            hashSet.add(fde.getDegree());
        }
        return hashSet;
    }

    public static String getDegreeVal(String name){
        for (FaultDegreeEnum fde : FaultDegreeEnum.values()){
            if (name.equals(fde.getName())){
                return fde.getDegree();
            }
        }
        return null;
    }

    public static String getNameVal(String grade) {
        for (FaultDegreeEnum fde : FaultDegreeEnum.values()) {
            if (fde.getDegree().equals(grade)) {
                return fde.getName();
            }
        }
        return null;
    }


    public static FaultDegreeEnum getEnum(String grade) {
        for (FaultDegreeEnum e : FaultDegreeEnum.values()) {
            if (e.getDegree().equals(grade)) {
                return e;
            }
        }
        return null;
    }
    static Set<String> set = new HashSet<String>();

    public static Set<String> getFaultDegreeSet() {
        set.add(DANGER.getDegree());
        set.add(SERIOUS.getDegree());
        set.add(GENERAL.getDegree());
        return set;
    }

    public static String selectName(String degree){
        for(FaultDegreeEnum fde : FaultDegreeEnum.values()){
            if (fde.getDegree().equals(degree)){
                return fde.getName();
            }
        }
        return null;
    }

}
