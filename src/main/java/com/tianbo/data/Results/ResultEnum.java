package com.tianbo.data.Results;

import com.tianbo.data.constants.HttpStatus;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/8
 */
public enum ResultEnum {
    SUCCESS(HttpStatus.SUCCESS,"请求成功～",Boolean.TRUE),
    FAIL(HttpStatus.BAD_REQUEST,"请求失败～",Boolean.FALSE),
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED,"该用户未登录",Boolean.FALSE);

    int code;
    String msg;
    Boolean successful;

    ResultEnum(int code, String msg,Boolean successful) {
        this.code = code;
        this.msg = msg;
        this.successful = successful;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public Boolean getSuccessful() {
        return successful;
    }
}
