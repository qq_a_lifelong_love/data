package com.tianbo.data.service;

import com.tianbo.data.entity.EquType;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/26
 */
public interface EquTypeService {


    /**
     *添加设备类型
     *
     * @param equType 设备类型的数据
     * @return 受影响的行数
     */
    void addEquType(EquType equType);

    /**
     * 根据设备类型的id删除当前设备类型
     *
     * @param equTypeId 设备类型的id
     * @return 返回受影响的行数
     */
    void deleteEquType(Long equTypeId);

    /**
     * 修改设备类型
     * @param equType 设备类型对象
     * @return 受影响的行数
     */
    void updateEquType(EquType equType);


    /**
     * 根据设备类型的id查询当前设备的详细信息
     *
     * @param equTypeId 设备类型id
     * @return 设备类型对象的具体信息
     */
    EquType getByEquTypeId(Long equTypeId);
}
