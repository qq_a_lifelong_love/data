package com.tianbo.data.controller;

import com.tianbo.data.service.OssService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/31
 */
@Api(description = "文件上传-上传图片")
@RestController
@RequestMapping("/oss/fileoss")
public class OssController {
//http://localhost:8081/swagger-ui.html
    @Autowired
    private OssService ossService;

    //    上传头像方法
    @PostMapping
    public String uploadOssFile(MultipartFile file) {
//        先获取上传文件MultipartFile
//        返回上传到oss的路径
        String url = ossService.uploadFileImage(file);
        System.out.println(url);

//        return Result.ok().data("url", url);
        return url;
    }
}
