package com.tianbo.data.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.ZipUtil;
import cn.hutool.extra.cglib.CglibUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tianbo.data.entity.DetectFormula;
import com.tianbo.data.entity.DetectRules;
import com.tianbo.data.service.DataExportService;
import com.tianbo.data.service.DetectFormulaService;
import com.tianbo.data.service.DetectRulesService;
import com.tianbo.data.service.MinioService;
import com.tianbo.data.utils.StringUtils;
import com.tianbo.data.utils.SysUtils;
import com.tianbo.data.vo.ExportRuleVo;
import com.tianbo.data.vo.RuleScreenVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/5
 */
@Service
@Slf4j
public class DataExportServiceImpl implements DataExportService {


    @Value("${Custom-settings.temp-path}")
    private String tempFilePath;

    @Autowired
    private DetectRulesService detectRulesService;

    @Autowired
    private DetectFormulaService detectFormulaService;

    @Autowired
    private ThreadPoolTaskExecutor executor;

    @Autowired
    private MinioService minioService;


    /**
     * 根据筛选条件，导出诊断规则成zip形式
     *
     * @param ruleScreenVo 诊断规则导出excel的vo类
     * @return
     */
    @Override
    public String exportRules(RuleScreenVo ruleScreenVo) {
        String time = DateUtil.format(new Date(), DatePattern.PURE_DATETIME_MS_FORMAT);
//        定义拼接路径
//        String parentPath = tempFilePath + StringUtils.SLASH + SysUtils.getCurrentUserId() + StringUtils.SLASH + time + "诊断规则";
        String parentPath = tempFilePath + StringUtils.SLASH + StringUtils.SLASH + time + "诊断规则";
        //拼接筛选条件
        LambdaQueryWrapper<DetectRules> ruleQuery = Wrappers.lambdaQuery(DetectRules.class);
//       设置查询条件
        //ruleQuery.eq(DetectRulesPo::getBelongType, ruleScreenVo.getBelongType());
        if (StringUtils.isNotEmpty(ruleScreenVo.getIdArray())) {
            ruleQuery.in(DetectRules::getDetectRulesId, ruleScreenVo.getIdArray());
        }
        ruleScreenVo.setIdArray(ruleScreenVo.getIdArray());

//        List<DetectRules> ruleList = detectRulesService.list(ruleQuery);
        List<DetectRules> ruleList = detectRulesService.list(ruleScreenVo);
        if (StringUtils.isEmpty(ruleList)) {
            return null;
        }
        Map<String, List<DetectRules>> poMap = ruleList.stream().collect(Collectors.groupingBy(DetectRules::getBelongType));
        List<ExportRuleVo> list = new ArrayList<>();
        poMap.forEach((key, value) -> {
            List<ExportRuleVo> child = CglibUtil.copyList(value, ExportRuleVo::new);
            list.addAll(child);
        });
        //用于封装图片的集合
        List<List<String>> imageList = new ArrayList<>();
        //生成excel
        String excelPath = parentPath + StringUtils.SLASH + SysUtils.getFormatNowDate() + "诊断规则.xls";

        //每一行的数据
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.put(ExportRuleVo.F_BELONG_TYPE, ExportRuleVo.BELONG_TYPE);
        map.put(ExportRuleVo.F_EQU_TYPE_NAME, ExportRuleVo.EQU_TYPE_NAME);
        map.put(ExportRuleVo.F_DETECT_RULE_CODE, ExportRuleVo.DETECT_RULE_CODE);
        map.put(ExportRuleVo.F_DETECT_RULE_NAME, ExportRuleVo.DETECT_RULE_NAME);
        map.put(ExportRuleVo.F_IS_ONLY_RULE, ExportRuleVo.IS_ONLY_RULE);
        map.put(ExportRuleVo.F_ANAS, ExportRuleVo.ANAS);
        map.put(ExportRuleVo.F_IMAGE_NAME, ExportRuleVo.IMAGE_NAME);
        map.put(ExportRuleVo.F_DES, ExportRuleVo.DES);
        //提示诊断规则名称
        map.put(ExportRuleVo.F_MSG, ExportRuleVo.MSG);
        map.put(ExportRuleVo.F_RULE_CONTENT, ExportRuleVo.RULE_CONTENT);
        //DetectFormulaPo的筛选条件
        LambdaQueryWrapper<DetectFormula> lambdaQueryWrapper = Wrappers.lambdaQuery(DetectFormula.class);
        String imageName;
        for (int index = 0; index < list.size(); index++) {
            ExportRuleVo erv = list.get(index);
            //保存图片名称
            List<String> arrayList = new ArrayList<>();
            arrayList.add(0, erv.getBelongType());
            arrayList.add(1, erv.getEquTypeName());
            arrayList.add(2, erv.getDetectRulesName());
            arrayList.add(3, erv.getImageName());
            //这个就这样写吧，万一过程中出现意外换了桶
//            li.add(4,erv.getBucketName());
            arrayList.add(4, erv.getImagePath());
            imageList.add(arrayList);

            //获取诊断规则
            lambdaQueryWrapper.select(DetectFormula::getFaultDegreeName,
                                        DetectFormula::getFormulaDetail,
                                        DetectFormula::getFormulaDescribe,
                                        DetectFormula::getJudgementResult,
                                        DetectFormula::getFaultDescribe,
                                        DetectFormula::getHandleSuggestion);
            lambdaQueryWrapper.eq(DetectFormula::getDetectRulesId, erv.getDetectRulesId());
            //获取公式数据后清空筛选条件

            DetectFormula detectFormula = new DetectFormula();
//            List<Map<String, Object>> formulaList = detectFormulaService.listMaps(lambdaQueryWrapper);
            List<DetectFormula> formulaList = detectFormulaService.getFormulaDescription(detectFormula.getDetectRulesId());
            lambdaQueryWrapper.clear();
            //封装image的名字
            imageName = erv.getBelongType() + StringUtils.SLASH + erv.getEquTypeName() + StringUtils.SLASH + erv.getImageName().substring(erv.getImageName().lastIndexOf(StringUtils.SLASH) + 1);

            erv.setImageName(imageName);
            if (StringUtils.isNotEmpty(formulaList)) {
                String jsonFormula = JSON.toJSONString(formulaList);
                //替换数据字段，适应其他系统
                jsonFormula = jsonFormula.replace(DetectFormula.COL_FAULT_DEGREE_NAME, ExportRuleVo.E_FAULT_DEGREE)
                        .replace(DetectFormula.COL_FORMULA_DETAIL, ExportRuleVo.E_DETECT_FORMULA)
                        .replace(DetectFormula.COL_FORMULA_DESCRIBE, ExportRuleVo.E_DES_OF_FORMULA)
                        .replace(DetectFormula.COL_FAULT_DESCRIBE, ExportRuleVo.E_TERMAL_IMAGE)
                        .replace(DetectFormula.COL_HANDLE_SUGGESTION, ExportRuleVo.E_HANDING_SUGGESTION);
                erv.setFormulaList(jsonFormula);
                log.info("导出一批诊断记录成功");
            }
            log.info("导出一条诊断规则成功");
        }
        SysUtils.writeExcel(excelPath, map, list);
        //------下载开始-------//
        threadImage(imageList, parentPath);
        //------下载结束-------//
        //压缩文件保存目录不能在被压缩的目录下
        ZipUtil.zip(parentPath, CharsetUtil.charset(CharsetUtil.GBK));
        return parentPath;
    }

    /**
     * 多线程下载的
     *
     * @param list
     * @param casePath
     */

    private void threadImage(List<List<String>> list, String casePath) {
        CountDownLatch latch = new CountDownLatch(list.size());
        for (List<String> img : list) {
            executor.execute(() -> {
                //此处可以放入待处理的业务
                //图片路径
                String imgPath = casePath + StringUtils.SLASH + img.get(0) + StringUtils.SLASH + img.get(1) + StringUtils.SLASH + img.get(3).substring(img.get(3).lastIndexOf(StringUtils.SLASH) + 1);
                try {
//                    InputStream is = minioSrv.getObject(img.get(4),img.get(3));
                    byte[] fileBytes = minioService.getFileBytes(img.get(4), img.get(3));
                    InputStream is = new ByteArrayInputStream(fileBytes);
                    if (StringUtils.isNotNull(is)) {
                        FileUtil.touch(imgPath);
                        File file = new File(imgPath);
                        OutputStream os = new FileOutputStream(file);
                        IoUtil.copy(is, os);
                        os.close();
                        log.info("下载图片成功");
                    }
                    is.close();
                } catch (Exception e) {
                    log.error("下载图片失败，原因为：", e);
                } finally {
                    latch.countDown();
                }
            });
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            log.info("计数器出错:", e);
        }
    }
}
