package com.tianbo.data.service;

import lombok.SneakyThrows;

import java.io.InputStream;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/5
 */
public interface MinioService {
    /**
     * 删除文件
     * @param bucketName
     * @param fileName
     * @return
     */
    boolean removeObject(String bucketName, String fileName);

    /**
     * 通过InputStream上传对象，远端文件中心中存储的的文件名为上传流文件的md5值，保证远端存储的文件唯一性，业务端使用的使用可以根据md5进行文件的预览url获取或者流获取。
     *
     * @param bucketName  存储桶名称
     * @param stream      要上传的流
     * @param objectName  minio中文件名：取MD5
     * @param contentType 文件类型
     * @return
     */
    String putObject(String bucketName, InputStream stream, String objectName, String contentType);

    /**
     *上传文件
     *
     * @param bucketName 存储桶名称
     * @param fileBytes
     * @param objectName  minio中文件名：取MD5
     * @param contentType  文件存储类型
     * @return
     */
    String putFileBytes(String bucketName, byte[] fileBytes, String objectName, String contentType);

    /**
     * 生成缩略图并保存
     * @param bucketName
     * @param objectName
     * @param fileBytes
     * @return
     */
    byte[] saveThumbnail(String bucketName, String objectName, byte[] fileBytes);

    /**
     * 生成一个给HTTP GET请求用的presigned URL。
     * 浏览器/移动端的客户端可以用这个URL进行下载，即使其所在的存储桶是私有的。这个presigned URL可以设置一个失效时间，默认值是7天。
     *
     * @param bucketName 存储桶名称
     * @param objectName 存储桶里的对象名称
     * @param expires    失效时间（以秒为单位），默认是7天，不得大于七天
     * @return
     */
    String preSignedGetObject(String bucketName, String objectName, Integer expires);

    byte[] getFileBytes(String bucketName, String objectName);

    @SneakyThrows
    List<String> objectList(String bucketName, String folderName);


}
