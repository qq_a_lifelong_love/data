package com.tianbo.data.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/31
 */
public interface OssService {

//    上传头像到oss
    String uploadFileImage(MultipartFile file);
}
