package com.tianbo.data.controller;

import com.tianbo.data.Results.Result;
import com.tianbo.data.service.DetectFormulaService;
import com.tianbo.data.service.DetectRulesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/9
 */
@RestController
@RequestMapping("/detectRules")
@Api(description = "诊断规则的操作模块")
@CrossOrigin//可以解决跨域问题
@Slf4j
public class DetectRulesController {
// swagger的常规路径   http://localhost:8081/swagger-ui.html

    @Autowired
    private DetectRulesService detectRulesService;

    @Autowired
    private DetectFormulaService detectFormulaService;
/*
* 基本的增删改查已经测试完毕，故不在此增加代码了
* */

    @ApiIgnore
    @Deprecated
    @PostMapping("/recover")
    @ApiOperation("恢复诊断规则")
    public Result<?> recover(@RequestParam(value = "idArray") @ApiParam(value = "规则id数组", required = true) Long [] idArray,@RequestParam(value = "branch",required = false) @ApiParam(value = "是否恢复规则下的诊断公式，true-恢复,false-不恢复") boolean branch){
        List<Long> list = new ArrayList<>(Arrays.asList(idArray));
        detectRulesService.recover(list);
        if (branch){
            detectFormulaService.recoverByRuleId(list);
        }
        return Result.success("恢复成功");
    }

    @PostMapping("/updateImage")
    @ApiOperation("更改图片")
//    @PrePerm(express = "tbcloud:detectRules:updateImage")
    public Result<String> updateImage(@RequestParam(value = "file")
                                          @ApiParam(value = "图片",required = true) MultipartFile multipartFile,
                                      @RequestParam("id") @ApiParam(value = "诊断规则id",required = true) Long id){
        //要解决名称相同覆盖问题
        return detectRulesService.updateImage(multipartFile,id);
    }

}
