package com.tianbo.data.service;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/15
 */
public interface RpcRuleService {

    /**
     * 获取ruleCode
     */
    String createRuleCode(String belongType);

}
