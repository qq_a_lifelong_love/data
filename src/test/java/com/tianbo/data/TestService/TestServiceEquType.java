package com.tianbo.data.TestService;

import com.tianbo.data.entity.EquType;
import com.tianbo.data.service.EquTypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/26
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TestServiceEquType {

    @Autowired
    private EquTypeService equTypeService;

    /*增*/
    @Test
    public void addEquType() {

        EquType equType = new EquType();
        equType.setEquTypeCode("108");
        equType.setEquTypeName("name8");
        equType.setEquTypeAlias("other8");
        equType.setBelongType("8");
        equType.setRemark("备注测试");
        equTypeService.addEquType(equType);
    }

    /*删*/
    @Test
    public void deleteEquType() {
        equTypeService.deleteEquType(23l);
    }

    /*改*/
    @Test
    public void updateEquType() {

        EquType equType = new EquType();
        equType.setEquTypeId(40l);
        equType.setEquTypeCode("106");
        equType.setEquTypeName("name6");
        equType.setEquTypeAlias("name6");
        equType.setBelongType("2");
        equType.setSorted(0);
        equType.setCreateTime(equType.getCreateTime());
        equType.setUpdateTime(new Date());
        equType.setDeleted(0);
        equType.setVersion(1);
        equType.setRemark("测试1");
        equTypeService.updateEquType(equType);
    }

    /*查*/
    @Test
    public void getByEquTypeId() {
        EquType equType = equTypeService.getByEquTypeId(40l);
        System.out.println(equType);
    }

}
