package com.tianbo.data.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/7
 */
//@RefreshScope
@Data
@ConfigurationProperties(prefix = "tianbo.minio")
@Component
public class MinioProperties {

    /**
     * 认证用户
     */
    private String accessKey;
    /**
     * 认证密码
     */
    private String secretKey;
    /**
     * 需要创建的 bucket
     */
    private String buckets;
    /**
     * minio主机地址
     */
    private String endpoint;
}
