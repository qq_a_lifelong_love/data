package com.tianbo.data.utils;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.tianbo.data.constant.TBConstant;
import com.tianbo.data.constants.SysConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Sheet;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/2
 */
@Slf4j
public class SysUtils {

    /**
     * 用户信息 缓存key
     *
     * @param userid
     * @return
     */
/*    public static String getUserinfoKey(String userid) {
        return TBConstant.LOGIN_INFO_CACHE + userid;
    }*/

    /**
     * 获取当前日期格式化字符串(yyyyMMddHHmmss)
     */
    public static String getFormatNowDate() {
        return DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN);
    }

    /**
     * 获取当前年月日
     * @return
     */
/*    public static String getCreateDate(){
        return DateUtil.format(new Date(), DatePattern.NORM_DATE_PATTERN);
    }*/

    /**
     * 获取当前年月日
     * @return
     */
    public static String getDate(){
        return DateUtil.format(new Date(), DatePattern.NORM_DATETIME_FORMATTER);
    }

    /**
     * 随机数
     */
/*    public static int getRandom(){
        Random random = new Random();
        return random.nextInt(10000000 - 10) + 10 + 1;
    }*/




    /**
     * minio获取的inputStream转换成byte[]
     */
//    public static byte[] getFileByteArr(InputStream input) {
//        try {
//            ByteArrayOutputStream output = new ByteArrayOutputStream();
//            byte[] buffer = new byte[4096];
//            int n = 0;
//            while (-1 != (n = input.read(buffer))) {
//                output.write(buffer, 0, n);
//            }
//            output.close();
//            input.close();
//            return output.toByteArray();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

//    public static Long getCurrentUserId() {
//        try {
//            return Long.valueOf(UserUtils.getUserid());
//        } catch (Exception e) {
//            log.error("获取用户id异常 MSG > {}", e.getMessage());
//            return SysConstants.ID_ADMIN;
//        }
//    }

    /**
     * @param response
     * @param contentType    "application/msword"  "application/vnd.ms-excel"   "image/jpeg"
     * @param file
     * @param outputFileName
     * @throws IOException
     */
//    public static void responseFile(HttpServletResponse response, String contentType, File file, String outputFileName) {
//        if (!StringUtils.isEmpty(contentType)) {
//            response.setContentType(contentType);
//        }
//        try {
//            response.setHeader("Content-Disposition", "attachment;fileName=" + new String(outputFileName.getBytes("gb2312"), "ISO8859-1"));
//            response.setCharacterEncoding("UTF-8");
//            FileInputStream fis = new FileInputStream(file);
//            OutputStream fos = response.getOutputStream();
//            IoUtil.copy(fis, fos);
//            fis.close();
//            fos.close();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    /**
     * 数据写入excel(支持列宽自适应)
     *
     * @param excelFilePath  excel文件路径
     * @param headerAliasMap 表头别名对   writer.addHeaderAlias("tType", "输变配");
     * @param beanList       数据列表
     */
    public static void writeExcel(String excelFilePath, LinkedHashMap<String, String> headerAliasMap, List beanList) {
        ExcelWriter writer = ExcelUtil.getWriter(excelFilePath);

        //自定义标题别名
        Set<Map.Entry<String, String>> entries = headerAliasMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            writer.addHeaderAlias(entry.getKey(), entry.getValue());
        }

        // 默认的，未添加alias的属性也会写出，如果想只写出加了别名的字段，可以调用此方法排除之
        writer.setOnlyAlias(true);
        // 一次性写出内容，使用默认样式，强制输出标题
        writer.write(beanList);

        int colSize = headerAliasMap.size();
        Sheet sheet = writer.getSheet();
        // 列宽自适应
        for (int i = 0; i < colSize; i++) {
            sheet.autoSizeColumn(i);

            int width = sheet.getColumnWidth(i) + 2000;
            //查看源码发现，宽度过大时，会抛异常
            if (width > 65280) {
                continue;
            }
            sheet.setColumnWidth(i, width);
        }

        //标题行
        writer.setRowHeight(0, 25);

        //数据行高
        for (int i = 0; i < beanList.size(); i++) {
            writer.setRowHeight(i + 1, 20);
        }

        // 关闭writer，释放内存
        writer.close();
    }


    public static byte[] getBufferedImageBytes(InputStream in, Float quality) {
        //这个代码对jdk版本有依赖，容易出bug
        BufferedImage originImage = null;
        try {
            originImage = ImageIO.read(in);
            int width = originImage.getWidth();
            int height = originImage.getHeight();
            //类型需要仔细分析
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
            image.getGraphics().drawImage(originImage, 0, 0, null);
            if (quality == null) {
                quality = 0.8f;
            }
            byte[] imageBytes = getBufferedImageBytes(image, quality);


            return imageBytes;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    public static byte[] getBufferedImageBytes(BufferedImage source, float quality) {
        if (null == source) {
            return null;
        }
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        JPEGImageEncoder jpedImageEncoder = JPEGCodec.createJPEGEncoder(output);
        JPEGEncodeParam param = jpedImageEncoder.getDefaultJPEGEncodeParam(source);
        param.setQuality(quality, true);
        jpedImageEncoder.setJPEGEncodeParam(param);
        try {
            jpedImageEncoder.encode(source);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return output.toByteArray();
    }


    /**
     * @param response
     * @param contentType    "application/msword"  "application/vnd.ms-excel"   "image/jpeg"
     * @param file
     * @param outputFileName
     * @throws IOException
     */
    public static void responseFile(HttpServletResponse response, String contentType, File file, String outputFileName) {
        if (!StringUtils.isEmpty(contentType)) {
            response.setContentType(contentType);
        }
        try {
            response.setHeader("Content-Disposition", "attachment;fileName=" + new String(outputFileName.getBytes("gb2312"), "ISO8859-1"));
            response.setCharacterEncoding("UTF-8");
            FileInputStream fis = new FileInputStream(file);
            OutputStream fos = response.getOutputStream();
            IoUtil.copy(fis, fos);
            fis.close();
            fos.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static Long getCurrentUserId() {
        try {
            return Long.valueOf(UserUtils.getUserid());
        } catch (Exception e) {
            log.error("获取用户id异常 MSG > {}", e.getMessage());
            return SysConstants.ID_ADMIN;
        }
    }

}
