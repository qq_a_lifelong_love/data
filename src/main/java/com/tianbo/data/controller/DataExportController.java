package com.tianbo.data.controller;

import cn.hutool.core.io.FileUtil;
import com.tianbo.data.service.DataExportService;
import com.tianbo.data.utils.StringUtils;
import com.tianbo.data.utils.SysUtils;
import com.tianbo.data.vo.RuleScreenVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/9
 */
@RestController
@RequestMapping("/dataExport")
@Api(description = "数据导出接口")
public class DataExportController {


    @Autowired
    private DataExportService dataExportService;

    @PostMapping("/exportRules")
    @ApiOperation("根据筛选条件导出诊断规则成zip")
    public void exportRules(@RequestBody RuleScreenVo ruleScreenVo, HttpServletResponse response){
        String path = dataExportService.exportRules(ruleScreenVo);
        if (StringUtils.isNotEmpty(path)){
            File file = new File(path + ".zip");
            SysUtils.responseFile(response,"",file,file.getName());
            FileUtil.del(file);
            FileUtil.del(path);
        }
    }
}
