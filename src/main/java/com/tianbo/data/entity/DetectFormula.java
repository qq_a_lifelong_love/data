package com.tianbo.data.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import lombok.*;

import java.io.Serializable;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/23
 */
@Api(description = "诊断公式表")
@EqualsAndHashCode(callSuper = true)
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "tb_detect_formula")
public class DetectFormula extends BaseEntity implements Serializable {

//    private static final long serialVersionUID = 1L;
    /**
     * 诊断公式ID
     */
//    @TableId(value = "formula_id", type = IdType.ID_WORKER_STR)
    @TableId(value = "formula_id")
    private Long formulaId;

    /**
     * 检测规则 ID
     */
//    @TableField(value = "detect_rules_id")
    private Long detectRulesId;

    /**
     * 缺陷等级ID
     */
//    @TableField(value = "fault_degree_id")
    private Long faultDegreeId;

    /**
     * 缺陷等级名称
     */
//    @TableField(value = "fault_degree_name")
    private String faultDegreeName;

    /**
     * 缺陷等级编码
     * 1-危及   2-严重   3-一般
     */
//    @TableField(value = "fault_degree_code")
    private String faultDegreeCode;

    /**
     * 公式编码
     */
//    @TableField(value = "formula_code")
    private String formulaCode;

    /**
     * 公式名称
     */
//    @TableField(value = "formula_name")
    private String formulaName;

    /**
     * 诊断公式
     */
//    @TableField(value = "formula_detail")
    private String formulaDetail;

    /**
     * 所属类型（0-1-2：变电-输电-配电，其它无效）
     */
  /*  @TableField(value = "belong_type")
    private String belongType;*/

    /**
     * 诊断结果(故障特征)
     */
//    @TableField(value = "judgement_result")
    private String judgementResult;

    /**
     * 故障特征文字描述（热像特征）
     */
//    @TableField(value = "fault_describe")
    private String faultDescribe;

    /**
     * 公式描述
     */
//    @TableField(value = "formula_describe")
    private String formulaDescribe;

    /**
     * 处理建议
     */
//    @TableField(value = "handle_suggestion")
    private String handleSuggestion;

    /**
     * 排序
     */
//    @TableField(value = "sorted")
    private Integer sorted;

    public static final String COL_FORMULA_ID = "formula_id";

    public static final String COL_DETECT_RULES_ID = "detect_rules_id";

    public static final String COL_FAULT_DEGREE_ID = "fault_degree_id";

    public static final String COL_FAULT_DEGREE_NAME = "fault_degree_name";

    public static final String COL_FAULT_DEGREE_CODE = "fault_degree_code";

    public static final String COL_FORMULA_CODE = "formula_code";

    public static final String COL_FORMULA_NAME = "formula_name";

    public static final String COL_FORMULA_DETAIL = "formula_detail";

    public static final String COL_BELONG_TYPE = "belong_type";

    public static final String COL_JUDGEMENT_RESULT = "judgement_result";

    public static final String COL_FAULT_DESCRIBE = "fault_describe";

    public static final String COL_FORMULA_DESCRIBE = "formula_describe";

    public static final String COL_HANDLE_SUGGESTION = "handle_suggestion";

    public static final String COL_SORTED = "sorted";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_CREATOR = "creator";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_UPDATER = "updater";

    public static final String COL_DELETED = "deleted";

    public static final String COL_VERSION = "version";

    public static final String COL_REMARK = "remark";

}
