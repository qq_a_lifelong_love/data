package com.tianbo.data.Results;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/1
 */
//统一返回结果的类
@ApiModel(value = "响应实体")
public class Result<T> implements Serializable {

    /**
     * 状态码:请求成功或者失败
     */
    @ApiModelProperty(value = "响应状态码")
    private int code;
    /**
     * 返回信息
     */
    @ApiModelProperty(value = "响应信息")
    private String message;
    /**
     * 返回数据
     */
    @ApiModelProperty(value = "响应数据")
    private T data;
    /**
     * 返回数据总数
     */
    @ApiModelProperty(value = "数据总数")
    private Long total;

    /**
     * 请求成功，或者访问被拒绝
     */
    @ApiModelProperty(value = "响应结果")
    private Boolean successful;

    public Result(int code, String message, T data, Long total, Boolean successful) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.total = total;
        this.successful = successful;
    }

    public static <T> Result<T> ok() {
        return Result.common(ResultEnum.SUCCESS);
    }

    public static <T> Result<T> ok(T data) {
        return Result.<T>builder().withEnum(ResultEnum.SUCCESS).withObj(data).build();
    }

    public static <T> Result<T> ok(T data, Long total) {
        return Result.<T>builder().withSuccessful(Boolean.TRUE).withEnum(ResultEnum.SUCCESS).withObj(data).withTotal(total).build();
    }


    public static <T> Result<T> common(int code, String message, Boolean successful, T data, Long total){
        return Result.<T>builder().withSuccessful(successful).withCode(code).withObj(data).withTotal(total).withMsg(message).build();
    }

    public static <T> Result<T> common(ResultEnum resultEnum){
        return Result.<T>builder().withEnum(resultEnum).build();
    }
    public static <T> Result<T> common(ResultEnum resultEnum, T data, Long total){
        return Result.<T>builder().withEnum(resultEnum).withObj(data).withTotal(total).build();
    }

    /**
     * 请求成功，访问拒绝
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> refuse() {
        return Result.<T>builder().withSuccessful(Boolean.FALSE).withEnum(ResultEnum.SUCCESS).build();
    }

    public static <T> Result<T> refuse(String message) {
        return Result.<T>builder().withSuccessful(Boolean.FALSE).withCode(ResultEnum.SUCCESS.getCode()).withMsg(message).build();
    }

    public static <T> Result<T> success(String message) {
        return Result.<T>builder().withSuccessful(Boolean.TRUE).withCode(ResultEnum.SUCCESS.getCode()).withMsg(message).build();
    }

    /**
     * 请求失败
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> fail() {
        return Result.common(ResultEnum.FAIL);
    }

    public static <T> Result<T> fail(String message) {
        return Result.<T>builder().withSuccessful(Boolean.FALSE).withCode(ResultEnum.FAIL.getCode()).withMsg(message).build();
    }

    public static <T> Result<T> fail(int code, String message) {
        return Result.<T>builder().withSuccessful(Boolean.FALSE).withCode(code).withMsg(message).build();
    }


    private static <T> RestResultBuilder<T> builder() {
        return new RestResultBuilder<>();
    }

    private static class RestResultBuilder<T> {
        /**
         * 状态码
         */
        private int code;

        /**
         * 提示信息
         */
        private String msg;

        /**
         * 数据条数
         */
        private Long total;

        /**
         * 结果容器
         */
        private T data;
        /**
         * 是否成功
         */
        private Boolean successful;

        private RestResultBuilder<T> withCode(int code) {
            this.code = code;
            return this;
        }

        private RestResultBuilder<T> withMsg(String msg) {
            this.msg = msg;
            return this;
        }

        private RestResultBuilder<T> withTotal(Long total) {
            this.total = total;
            return this;
        }

        private RestResultBuilder<T> withObj(T data) {
            this.data = data;
            return this;
        }

        private RestResultBuilder<T> withSuccessful(Boolean successful) {
            this.successful = successful;
            return this;
        }

        private RestResultBuilder<T> withEnum(ResultEnum resultEnum) {
            this.code = resultEnum.getCode();
            this.msg = resultEnum.getMsg();
            this.successful = resultEnum.getSuccessful();
            return this;
        }

        private Result<T> build() {
            return new Result<>(code, msg, data, total, successful);
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Boolean getSuccessful() {
        return successful;
    }

    public void setSuccessful(Boolean successful) {
        this.successful = successful;
    }
}