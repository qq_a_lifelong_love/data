package com.tianbo.data.TestService;

import com.tianbo.data.entity.DetectRules;
import com.tianbo.data.entity.EquType;
import com.tianbo.data.service.DetectRulesService;
import com.tianbo.data.vo.RuleTreeVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/30
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TestServiceDetectRules {

    @Autowired
    private DetectRulesService detectRulesService;


    /*增*/
    @Test
    public void addDetectRules() {
        DetectRules detectRules = new DetectRules();
        EquType equType = new EquType();

        detectRules.setDetectRulesCode("诊断规则编码");
        detectRules.setDetectRulesName("诊断规则名称");

//        detectRules.setEquTypeId(22l);
//        detectRules.setEquTypeName("设备类型名称");
        detectRules.setEquTypeId(equType.getEquTypeId());
        detectRules.setEquTypeName(equType.getEquTypeName());
//        detectRules.setAnas("分析字符串");
        detectRules.setImageName("图片名称");
        detectRules.setImageStorageType("图片存储类型");
        detectRules.setImagePath("图片存储桶");
        detectRules.setBelongType("0");
        detectRules.setDesOfJudgement("诊断判据");
        detectRules.setSorted(33);
        detectRules.setRemark("备注");
        detectRulesService.addDetectRules(detectRules);
    }

    /*删*/
    @Test
    public void deleteDetectRules() {
        detectRulesService.deleteDetectRules(1l);
    }

    /*改*/
    @Test
    public void updateDetectRules() {
        DetectRules detectRules = new DetectRules();
        detectRules.setDetectRulesId(21l);
        detectRules.setDetectRulesCode("诊断规则编码22");
        detectRules.setDetectRulesName("诊断规则名称22");
//        detectRules.setEquTypeId(22l);
//        detectRules.setEquTypeName("设备类型名称22");
//        detectRules.setAnas("分析字符串");
        detectRules.setImageName("图片名称22");
        detectRules.setImageStorageType("图片存储类型22");
        detectRules.setImagePath("图片存储桶22");
        detectRules.setBelongType("0");
        detectRules.setDesOfJudgement("诊断判据2");
        detectRules.setSorted(22);
        detectRules.setVersion(1);
        detectRules.setRemark("备注2");
        detectRulesService.updateDetectRules(detectRules);
    }

    /*查*/
    @Test
    public void getDetectRulesById() {
        DetectRules result = detectRulesService.getDetectRulesById(21l);
        System.out.println(result);
    }

    /* 根据设备类型的id查询诊断规则的详细信息*/
    @Test
    public void getDetectRulesByEquTypeId() {
        List<DetectRules> result = detectRulesService.getDetectRulesByEquTypeId(22l);
        result.forEach(System.out::println);
    }


    /*树状结构查询*/
    @Test
    public void rulesTree(){
//        List<RuleTreeVo> ruleTreeVos = detectRulesService.rulesTree("0", 1470312842841907200L);
        List<RuleTreeVo> ruleTreeVos = detectRulesService.rulesTree("0", 23l);
        ruleTreeVos.forEach(System.out::println);
    }
}
