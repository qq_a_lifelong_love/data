package com.tianbo.data.TestMapper;

import com.tianbo.data.entity.DetectRules;
import com.tianbo.data.mapper.DetectRulesMapper;
import com.tianbo.data.vo.EquTypeVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/30
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class DetectRulesMapperTest {

    @Autowired
    private DetectRulesMapper detectRulesMapper;


    /*增*/
    @Test
    public void insertDetectRules() {

        DetectRules detectRules = new DetectRules();
        detectRules.setDetectRulesCode("诊断规则编码");
        detectRules.setDetectRulesName("诊断规则名称");
        detectRules.setEquTypeId(22l);                  //=========
        detectRules.setEquTypeName("设备类型名称");       //==========
//        detectRules.setAnas("分析字符串");
        detectRules.setImageName("图片名称");
        detectRules.setImageStorageType("图片存储类型");
        detectRules.setImagePath("图片存储桶");
        detectRules.setBelongType("0");
        detectRules.setDesOfJudgement("诊断判据");
        detectRules.setSorted(33);
        detectRules.setCreateTime(new Date());
        detectRules.setUpdateTime(new Date());
        detectRules.setDeleted(0);
        detectRules.setVersion(0);
        detectRules.setRemark("备注");
        Integer integer = detectRulesMapper.insertDetectRules(detectRules);
        System.out.println(integer);
    }

    /*删*/
    @Test
    public void deleteDetectRulesById() {
        Integer integer = detectRulesMapper.deleteDetectRulesById(1l);
        System.out.println(integer);
    }

    /*改*/
    @Test
    public void updateDetectRules() {
        DetectRules detectRules = new DetectRules();
        detectRules.setDetectRulesId(1l);
        detectRules.setDetectRulesCode("诊断规则编码1");
        detectRules.setDetectRulesName("诊断规则名称1");
        detectRules.setEquTypeId(22l);
        detectRules.setEquTypeName("设备类型名称");
//        detectRules.setAnas("分析字符串");
        detectRules.setImageName("图片名称");
        detectRules.setImageStorageType("图片存储类型");
        detectRules.setImagePath("图片存储桶");
        detectRules.setBelongType("0");
        detectRules.setDesOfJudgement("诊断判据");
        detectRules.setSorted(33);
        detectRules.setDeleted(0);
        detectRules.setVersion(0);
        detectRules.setRemark("备注");
        Integer integer = detectRulesMapper.updateDetectRules(detectRules);
        System.out.println(integer);
    }

    /*只修改图片*/
    @Test
    public void updateImageById() {
        DetectRules detectRules = new DetectRules();
        detectRules.setDetectRulesId(21l);
        detectRules.setImageName("图片新名字");
        detectRules.setImageStorageType("新的图片存储类型");

        detectRules.setDetectRulesCode(detectRules.getDetectRulesCode());
        detectRules.setDetectRulesName(detectRules.getDetectRulesName());
        detectRules.setEquTypeId(detectRules.getEquTypeId());
        detectRules.setEquTypeName(detectRules.getEquTypeName());
        detectRules.setImagePath(detectRules.getImagePath());
        detectRules.setBelongType(detectRules.getBelongType());
        detectRules.setDesOfJudgement(detectRules.getDesOfJudgement());
        detectRules.setSorted(detectRules.getSorted());
        detectRules.setDeleted(detectRules.getDeleted());
        detectRules.setVersion(detectRules.getVersion());
        detectRules.setRemark(detectRules.getRemark());
        detectRules.setCreateTime(detectRules.getCreateTime());
        detectRules.setUpdateTime(new Date());

        detectRulesMapper.updateImageById(detectRules);
    }

    /*查*/
    @Test
    public void findDetectRulesById() {
        DetectRules detectRules = detectRulesMapper.findDetectRulesById(2l);
        System.out.println(detectRules);
    }

    /*查*/
    @Test
    public void getOne1() {
        DetectRules detectRules = detectRulesMapper.getOne1(2l);
        System.out.println(detectRules);
    }

    /*根据设备类型id查询诊断规则*/
    @Test
    public void findDetectRulesByEquTypeId() {
        List<DetectRules> results = detectRulesMapper.findDetectRulesByEquTypeId(22l);
        results.forEach(System.out::println);
    }

    /**
     * 根据排序查找equType
     */
    @Test
    public void selectTypeList() {
        List<EquTypeVo> equTypeVos = detectRulesMapper.selectTypeList("0");
        equTypeVos.forEach(System.out::println);
    }

    @Test
    public void selectTypeAndTypeIdList() {
        List<DetectRules> list = detectRulesMapper.selectTypeAndTypeIdList("0", 23l);
        list.forEach(System.out::println);
    }


}
