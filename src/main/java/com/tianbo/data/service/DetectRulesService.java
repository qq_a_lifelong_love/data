package com.tianbo.data.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tianbo.data.Results.Result;
import com.tianbo.data.entity.DetectRules;
import com.tianbo.data.entity.EquType;
import com.tianbo.data.vo.RuleScreenVo;
import com.tianbo.data.vo.RuleTreeVo;
import com.tianbo.data.vo.SearchVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/30
 */
public interface DetectRulesService {

//    void save(DetectRules detectRules);

    /**
     * 添加診斷规则
     *
     * @param detectRules 诊断规则对象
     */
    void addDetectRules(DetectRules detectRules);

    /**
     * 删除诊断规则
     *
     * @param detectRulesId 诊断规则的id
     */
    void deleteDetectRules(Long detectRulesId);

    /**
     * 修改诊断规则
     *
     * @param detectRules 诊断规则对象
     */
    void updateDetectRules(DetectRules detectRules);

    /**
     * 查询诊断规则
     *
     * @param detectRulesId 诊断规则的id
     * @return
     */
    DetectRules getDetectRulesById(Long detectRulesId);

    /**
     * 查询诊断规则
     *
     * @param detectRulesId 诊断规则的id
     * @return
     */
    DetectRules getOne1(Long detectRulesId);

    /**
     * 根据设备类型的id查询诊断规则的详细信息
     *
     * @param equTypeId
     * @return
     */
    List<DetectRules> getDetectRulesByEquTypeId(Long equTypeId);

    /**
     * 诊断规则左侧树结构
     *
     * @param belongType 所属类型（0-1-2：变电-输电 -配电，其它无效）
     * @param equTypeId  设备类型ID
     * @return
     */
    List<RuleTreeVo> rulesTree(String belongType, Long equTypeId);

    /**
     * 图片上传——只修改图片
     *
     * @param multipartFile 上传图片文件
     * @param detectRulesId 诊断规则id
     * @return
     */
    Result<String> updateImage(MultipartFile multipartFile, Long detectRulesId);

    /**
     * 查询诊断规则
     *
     * @param ruleScreenVo
     * @return
     */
    List<DetectRules> list(RuleScreenVo ruleScreenVo);

    /**
     * 逻辑恢复
     * @param list
     * @return
     */
    Integer recover(List<Long> list);

    /**
     * 搜索
     */
/*
    List<DetectRules> search(SearchVo searchVo);
*/

    Result<?> uploadZip(File mergeFile);


}
