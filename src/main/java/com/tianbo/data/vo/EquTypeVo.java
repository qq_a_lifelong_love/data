package com.tianbo.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/1
 */

@Data
@ApiModel("设备类型的vo类")
@AllArgsConstructor
@NoArgsConstructor
public class EquTypeVo {

    /**
     * 设备类型ID
     */
    @ApiModelProperty("设备类型ID")
    private Long equTypeId;

    /**
     * 设备类型名称
     */
    @ApiModelProperty("设备类型名称")
    private String equTypeName;

    /**
     * 设备所属类型（0-1-2：变电-输电-配电，其它无效）
     */
    @ApiModelProperty(value = "设备所属类型（0-1-2：变电-输电-配电，其它无效）")
    private String belongType;

    @ApiModelProperty(value = "排序，不能大于2147483647")
    private Integer sorted;

}
