package com.tianbo.data.service;

import com.tianbo.data.entity.DetectFormula;
import com.tianbo.data.entity.DetectRules;

import java.util.List;


/**
 * @Author : 宋军辉
 * @Date : 2022/8/29
 */
public interface DetectFormulaService {

    /**
     * 添加诊断公式
     *
     * @param detectFormula 诊断公式的数据
     * @return 受影响的行数
     */
    void addDetectFormula(DetectFormula detectFormula);

    /**
     * 根据诊断公式的id删除当前的诊断公式
     *
     * @param formulaId 诊断公式的id
     * @return 返回受影响的行数
     */
    void deleteDetectFormula(Long formulaId);

    /**
     * 修改诊断公式信息
     *
     * @param detectFormula 诊断公式的对象
     * @return 受影响的行数
     */
    void updateDetectFormula(DetectFormula detectFormula);


    /**
     * 根据诊断公式的id查询当前诊断公式的详细信息
     *
     * @param formulaId 诊断公式的id
     * @return 诊断公式对象的具体信息
     */
    DetectFormula getDetectFormulaById(Long formulaId);

    /**
     * 根据诊断规则的id查询诊断公式的详细信息
     *
     * @param detectRulesId 诊断规则的id
     * @return 按照列表显示诊断公式的详细信息
     */
    List<DetectFormula> getDetectFormulaByDetectRulesId(Long detectRulesId);



    List<DetectFormula> getFormulaDescription(Long detectRulesId);

    /**
     * 根据诊断规则id恢复
     * @param list
     * @return
     */
    int recoverByRuleId(List<Long> list);

    /**
     * 不分页筛选查询
     * @return
     */
/*    List<DetectFormula> list(DetectFormula detectFormula);*/


}
