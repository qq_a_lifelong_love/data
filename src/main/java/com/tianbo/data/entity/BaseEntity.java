package com.tianbo.data.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.Version;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/23
 */
@Api(description = "基类")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BaseEntity implements Serializable {

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 创建人
     */

//    @TableField(value = "creator", fill = FieldFill.INSERT)
    private Long creator;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 更新人
     */
//    @TableField(value = "updater", fill = FieldFill.INSERT_UPDATE)
    private Long updater;

    /**
     * 软删除（1：已删除）
     */
    @ApiModelProperty(value = "逻辑删除 1（true）已删除，    0（false）未删除")
    @TableField(value = "deleted", fill = FieldFill.INSERT)
    @TableLogic//逻辑删除必备的注解
    private Integer deleted;

    /**
     * 乐观锁版本号
     */
    @TableField(value = "version", fill = FieldFill.INSERT)
    @Version
    private Integer version;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

}
