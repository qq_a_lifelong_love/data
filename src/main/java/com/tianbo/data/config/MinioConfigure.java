package com.tianbo.data.config;

import com.tianbo.data.properties.MinioProperties;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.errors.MinioException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/7
 */
@Configuration
@Slf4j
public class MinioConfigure {

    @Bean
    public MinioClient minioClient(MinioProperties minioProperties) {
        MinioClient minioClient = MinioClient.builder()
                .credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey())
                .endpoint(minioProperties.getEndpoint())
                .build();
        String bucket = minioProperties.getBuckets();
        BucketExistsArgs bucketExistsArgs = BucketExistsArgs.builder()
                .bucket(bucket)
                .build();
        try {
            boolean exists = minioClient.bucketExists(bucketExistsArgs);
            if (!exists) {
                MakeBucketArgs makeBucketArgs = MakeBucketArgs.builder().bucket(bucket).build();
                minioClient.makeBucket(makeBucketArgs);
                log.info("create bucket : {} successful", bucket);

            } else {
                log.info("bucket : {} is exists", bucket);
            }
        } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException ex) {
            log.error("create bucket : {} error", bucket, ex);
        }

        return minioClient;
    }
}
