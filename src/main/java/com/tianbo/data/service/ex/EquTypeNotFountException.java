package com.tianbo.data.service.ex;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/26
 */
public class EquTypeNotFountException extends ServiceException{
    public EquTypeNotFountException() {
        super();
    }

    public EquTypeNotFountException(String message) {
        super(message);
    }

    public EquTypeNotFountException(String message, Throwable cause) {
        super(message, cause);
    }

    public EquTypeNotFountException(Throwable cause) {
        super(cause);
    }

    protected EquTypeNotFountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
