package com.tianbo.data.TestMapper;

import com.tianbo.data.entity.DetectFormula;
import com.tianbo.data.mapper.DetectFormulaMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/26
 */

@SpringBootTest
@RunWith(SpringRunner.class)
public class DetectFormulaMapperTest {

    @Autowired
    private DetectFormulaMapper detectFormulaMapper;

    //    添加诊断公式
    @Test
    public void insertDetectFormula() {
        DetectFormula detectFormula = new DetectFormula();
        detectFormula.setDetectRulesId(20l);
        detectFormula.setFaultDegreeId(20l);
        detectFormula.setFaultDegreeName("缺陷等级名称");
        detectFormula.setFaultDegreeCode("缺陷等级编码");
        detectFormula.setFormulaCode("公式编码");
        detectFormula.setFormulaName("公式名称");
        detectFormula.setFormulaDetail("诊断公式");
        detectFormula.setJudgementResult("诊断结果(故障特征)");
        detectFormula.setFaultDescribe("故障特征文字描述（热像特征）");
        detectFormula.setFormulaDescribe("公式描述");
        detectFormula.setHandleSuggestion("处理建议");
        detectFormula.setSorted(20);
        detectFormula.setCreateTime(new Date());
        detectFormula.setUpdateTime(new Date());
        detectFormula.setDeleted(0);
        detectFormula.setVersion(0);
        detectFormula.setRemark("标记测试");


        Integer integer = detectFormulaMapper.insertDetectFormula(detectFormula);
        System.out.println(integer);
    }

    //  删除诊断公式
    @Test
    public void deleteByFormulaById() {
     detectFormulaMapper.deleteByFormulaId(10l);
    }

    //    修改诊断公式
    @Test
    public void updateByFormulaById() {
        DetectFormula detectFormula = new DetectFormula();
        detectFormula.setFormulaId(11l);
        detectFormula.setDetectRulesId(20l);
        detectFormula.setFaultDegreeId(20l);
        detectFormula.setFaultDegreeName("缺陷等级名称1");
        detectFormula.setFaultDegreeCode("缺陷等级编码1");
        detectFormula.setFormulaCode("公式编码1");
        detectFormula.setFormulaName("公式名称1");
        detectFormula.setFormulaDetail("诊断公式1");
        detectFormula.setJudgementResult("诊断结果(故障特征)");
        detectFormula.setFaultDescribe("故障特征文字描述（热像特征）");
        detectFormula.setFormulaDescribe("公式描述1");
        detectFormula.setHandleSuggestion("处理建议1");
        detectFormula.setSorted(20);
        detectFormula.setDeleted(0);
        detectFormula.setVersion(0);
        detectFormula.setRemark("标记测试1");
        detectFormulaMapper.updateByFormula(detectFormula);
    }


    //查询诊断公式
    @Test
    public void findByFormulaId() {
        DetectFormula byFormulaId = detectFormulaMapper.findByFormulaId(14l);
        System.out.println(byFormulaId);
    }
    ////////////////////////////////////////////////////

    //     根据诊断规则的id查询诊断公式
    @Test
    public void findByFormulaByRuleId() {
        List<DetectFormula> result = detectFormulaMapper.findByFormulaByRuleId(10l);
        result.forEach(System.out::println);
    }

    @Test
    public void selectFormulaDescription(){
        List<DetectFormula> list = detectFormulaMapper.selectFormulaDescription(11l);
        list.forEach(System.out::println);

    }

}
