package com.tianbo.data.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ZipUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tianbo.data.Results.Result;
import com.tianbo.data.entity.DetectFormula;
import com.tianbo.data.entity.DetectRules;
import com.tianbo.data.entity.EquType;
import com.tianbo.data.enums.FaultDegreeEnum;
import com.tianbo.data.enums.ImageEnum;
import com.tianbo.data.mapper.DetectFormulaMapper;
import com.tianbo.data.mapper.DetectRulesMapper;
import com.tianbo.data.service.*;
import com.tianbo.data.service.ex.DeleteException;
import com.tianbo.data.service.ex.DetectRulesNotFountException;
import com.tianbo.data.service.ex.InsertException;
import com.tianbo.data.utils.StringUtils;
import com.tianbo.data.utils.SysUtils;
import com.tianbo.data.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


import javax.annotation.Resource;
import java.io.File;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/30
 */
@Slf4j
@Service
public class DetectRulesServiceImpl implements DetectRulesService {

    @Value("${Custom-settings.temp-path}")
    private String tempFilePath;

    @Value("${tianbo.minio.buckets}")
    private String imagePath;


    @Value("${Custom-settings.ruleFolder}")
    private String ruleFolder;


    @Value("${Custom-settings.rulesReduceFolder}")
    private String rulesReduceFolder;

    @Autowired
    private DetectRulesMapper detectRulesMapper;

    @Autowired
    private EquTypeService equTypeService;

    @Autowired
    private MinioService minioService;

    @Value("${tianbo.minio.buckets}")
    private String bucketName;

    @Autowired
    private DetectRulesService detectRulesService;

    @Autowired
    private DetectFormulaService detectFormulaService;

    @Autowired
    private DetectFormulaMapper detectFormulaMapper;

//    @Autowired
    @Resource
    private RpcRuleService rpcRuleService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addDetectRules(DetectRules detectRules) {
        DetectRules results = detectRulesMapper.findDetectRulesById(detectRules.getDetectRulesId());
        if (results != null) {
            throw new InsertException("当前诊断公式已经存在，无需重复添加");
        }

        //创建诊断规则需要拿到设备类型的 equ_type_id 和  equ_type_name
//        detectRules.setEquTypeId(equType.getEquTypeId());
//        detectRules.setEquTypeName(equType.getEquTypeName());

//        补全日志信息
        detectRules.setCreateTime(new Date());
        detectRules.setUpdateTime(new Date());
        detectRules.setVersion(0);
        detectRules.setDeleted(0);
        Integer integer = detectRulesMapper.insertDetectRules(detectRules);
        if (integer == 0) {
            throw new InsertException("添加时产生未知的异常");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteDetectRules(Long detectRulesId) {
        DetectRules result = detectRulesMapper.findDetectRulesById(detectRulesId);
        if (result == null) {
            throw new DetectRulesNotFountException("当前诊断规则不存在");
        }
        Integer integer = detectRulesMapper.deleteDetectRulesById(detectRulesId);
        if (integer == 0) {
            throw new DeleteException("删除时产生未知的异常");
        }
    }

    /**
     * 修改诊断规则
     *
     * @param detectRules 诊断规则对象
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateDetectRules(DetectRules detectRules) {

        DetectRules results = detectRulesMapper.findDetectRulesById(detectRules.getDetectRulesId());
        if (results == null) {
            throw new InsertException("当前没有该诊断公式");
        }

        detectRulesMapper.updateDetectRules(detectRules);

    }

    @Override
    public DetectRules getDetectRulesById(Long detectRulesId) {
        DetectRules result = detectRulesMapper.findDetectRulesById(detectRulesId);
        if (result == null) {
            throw new DetectRulesNotFountException("没有查询到当前的诊断规则");
        }
        return result;
    }

    @Override
    public DetectRules getOne1(Long detectRulesId) {
        DetectRules result = detectRulesMapper.getOne1(detectRulesId);
        if (result == null) {
            throw new DetectRulesNotFountException("没有查询到当前的诊断规则");
        }
        return result;
    }

    ////////////////////

    @Override
    public List<DetectRules> getDetectRulesByEquTypeId(Long equTypeId) {

        List<DetectRules> list = detectRulesMapper.findDetectRulesByEquTypeId(equTypeId);

        return list;
    }

    /**
     * 诊断规则管理左侧树
     *
     * @param belongType 所属类型（0-1-2：变电-输电 -配电，其它无效）
     * @param equTypeId  设备类型ID
     * @return
     */
    @Override
    public List<RuleTreeVo> rulesTree(String belongType, Long equTypeId) {
        //预装返回结果类
        List<RuleTreeVo> list = new ArrayList<>();
        //查询出所有的设备类型
        Map<Long, List<DetectRules>> map;
        if (StringUtils.isNull(equTypeId)) {
//            变电，输电，配电
            List<EquTypeVo> typeList = detectRulesMapper.selectTypeList(belongType);
            if (StringUtils.isNotEmpty(typeList)) {
                //根据equTypeId进行分组
                map = queryHandler(belongType, equTypeId);
                list = typeList.stream().map(vo -> {
                    Long typeId = vo.getEquTypeId();
                    RuleTreeVo tree = new RuleTreeVo();
                    tree.setId(typeId);
                    tree.setLabel(vo.getEquTypeName());
                    //处理子集
                    List<RuleTreeVo> child = childHandler(map, typeId);
                    tree.setChildren(child);
                    return tree;
                }).collect(Collectors.toList());
            }
        } else {
            map = queryHandler(belongType, equTypeId);
            list = childHandler(map, equTypeId);
        }
        return list;
    }


    //封装子集
    private Map<Long, List<DetectRules>> queryHandler(String belongType, Long equTypeId) {
        Map<Long, List<DetectRules>> map = new HashMap<>();
//        //查找诊断规则的id和诊断规则名称
//        LambdaQueryWrapper<DetectRules> lambdaQueryWrapper = Wrappers.lambdaQuery(DetectRules.class);
//        //设置查询字段
//        lambdaQueryWrapper.select(DetectRules::getDetectRulesId,DetectRules::getDetectRulesName, DetectRules::getEquTypeId);
//        //设置查询条件
//        lambdaQueryWrapper.eq(DetectRules::getBelongType,belongType)
//                .eq(StringUtils.isNotNull(equTypeId), DetectRules::getEquTypeId, equTypeId)
//                .orderByDesc(DetectRules::getSorted);


        List<DetectRules> poList = detectRulesMapper.selectTypeAndTypeIdList(belongType, equTypeId);
        if (StringUtils.isNotEmpty(poList)) {
            //根据equTypeId进行分组
            map = poList.stream().collect(Collectors.groupingBy(DetectRules::getEquTypeId));
        }
        return map;
    }

    private List<RuleTreeVo> childHandler(Map<Long, List<DetectRules>> map, Long equTypeId) {
        List<RuleTreeVo> list = new ArrayList<>();
        if (map.containsKey(equTypeId)) {
            List<DetectRules> ruleList = map.get(equTypeId);
            list = ruleList.stream().map(po -> {
                RuleTreeVo ruleTreeVo = new RuleTreeVo();
                ruleTreeVo.setParentId(equTypeId);
                ruleTreeVo.setLabel(po.getDetectRulesName());
                ruleTreeVo.setId(po.getDetectRulesId());
                ruleTreeVo.setId(po.getDetectRulesId());
                return ruleTreeVo;
            }).collect(Collectors.toList());
        }
        return list;
    }

    /**
     * 上传图片——只修改图片
     *
     * @param file          上传图片文件
     * @param detectRulesId 诊断规则id
     * @return
     */
    @Override
    public Result<String> updateImage(MultipartFile file, Long detectRulesId) {
        if (file == null || file.isEmpty()) {
            return Result.fail("文件为空!");
        }
        String originalFilename = file.getOriginalFilename();
        if (!originalFilename.endsWith(".jpg")) {
            return Result.fail("请上传.jpg文件!");
        }
//        截取上传的图像的名称
//        String substring = originalFilename.substring(0, originalFilename.lastIndexOf("."));
//        根据id查询出数据
        DetectRules detectRules = detectRulesMapper.findDetectRulesById(detectRulesId);
//        设置存储桶的名称
        String bucket = StringUtils.isNotEmpty(detectRules.getImagePath()) ? detectRules.getImagePath() : imagePath;
        String imageName = ruleFolder + StringUtils.SLASH + detectRulesId + StringUtils.UNDERLINE + detectRules.getBelongType() + StringUtils.DOT + ImgUtil.IMAGE_TYPE_JPG;
//        截掉图片全称的小数点前边的部分（也就是保留图片的类型数据）
        String imageStorageType = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
//      上传新的图片，以前有同名的话这次会将以往的覆盖
        try {
            minioService.putFileBytes(bucket, file.getBytes(), imageName, file.getContentType());
//            生成缩略图
            String thumbnailName = rulesReduceFolder + StringUtils.SLASH + detectRulesId + StringUtils.UNDERLINE + detectRules.getBelongType() + StringUtils.DOT + ImgUtil.IMAGE_TYPE_JPG;
//            生成缩略图并保存
            minioService.saveThumbnail(bucket, thumbnailName, file.getBytes());
//            生成url路径
            String url = minioService.preSignedGetObject(bucket, thumbnailName, null);
            detectRules.setImagePath(bucket);
            detectRules.setImageName(imageName);
            detectRules.setImageStorageType(imageStorageType);
            detectRules.setUpdateTime(new Date());
            detectRulesMapper.updateImageById(detectRules);
            return Result.ok(url);

        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail("更改图片失败");
        }
    }


    @Override
    public List<DetectRules> list(RuleScreenVo ruleScreenVo) {
        List<DetectRules> idArray = detectRulesMapper.selectIdArray(ruleScreenVo.getBelongType(), ruleScreenVo.getIdArray()[0]);
        if (idArray == null) {
            return null;
        }
        return idArray;
    }

    /**
     * 逻辑恢复
     *
     * @param list
     * @return
     */
    @Override
    public Integer recover(List<Long> list) {
//        Long userId = SysUtils.getCurrentUserId();
        LocalDateTime time = LocalDateTime.now();
//        return detectRulesMapper.recover(list, userId, time);
        return detectRulesMapper.recover(list, time);
    }

    /**
     * 搜索
     *
     * @return
     */
/*    @Override
    public List<DetectRules> search(SearchVo searchVo) {
        return detectRulesMapper.selectByBelongType(searchVo.getBelongType(), searchVo.getEquTypeName());
    }*/
    @Override
    public Result<?> uploadZip(File file) {
        //解压到unzipDir初始目录
        File unzipDir = ZipUtil.unzip(file, Charset.forName("GBK"));
        //里面只有一个excel
        File excelFile = null;
        for (File item : FileUtil.loopFiles(unzipDir)) {
            if (item.getAbsolutePath().endsWith(".xls") || item.getAbsolutePath().endsWith(".xlsx")) {
                excelFile = item;
                break;
            }
        }

        if (excelFile == null) {
            //删除临时文件夹
            FileUtil.del(unzipDir);
            return Result.fail("压缩包中没有excel文件");
        }

        ExcelReader reader = ExcelUtil.getReader(excelFile);

        //处理压缩文件
        List<Map<String, Object>> list = reader.readAll();
        //这个是证明excel中无数据
        if (StringUtils.isEmpty(list)) {
            return Result.fail("excel文件中无数据");
        }
        //调用存储的方法，就是下面这个
        saveRulesAndFormulas(list, FileUtil.getParent(excelFile.getPath(), 1));
        reader.close();
        log.debug("上传压缩包成功");
        return Result.ok();
    }


    /**
     * 保存诊断公式和诊断规则
     *
     * @param list
     * @param initialPath
     */
    private void saveRulesAndFormulas(List<Map<String, Object>> list, String initialPath) {
//        LambdaQueryWrapper<DetectRules> lambdaQueryRule = Wrappers.lambdaQuery(DetectRules.class);
        DetectRules dr = new DetectRules();
        EquType equType;
        log.info("处理excel数据");
        //缩略图的保存文件夹
        //String thumbnailPath = initialPath + StringUtils.SLASH + rulesReduceFolder;

        int index = 2;
        //遍历保存
        List<DetectFormula> formulaList = new ArrayList<>();
        for (Map<String, Object> item : list) {
            //输变配
            String belongType = item.getOrDefault(ExportRuleVo.BELONG_TYPE, "").toString();
            if (StringUtils.isEmpty(belongType)) {
                log.info("无输变配");
                continue;
            }

            //设备类型
            String equTypeName = item.getOrDefault(ExportRuleVo.EQU_TYPE_NAME, "").toString();
            if (StringUtils.isEmpty(equTypeName)) {
                log.info("无设备类型");
                continue;
            }

            //诊断规则编码
            String detectRulesCode = item.getOrDefault(ExportRuleVo.DETECT_RULE_CODE, "").toString();
            if (StringUtils.isEmpty(detectRulesCode)) {
                detectRulesCode = rpcRuleService.createRuleCode(belongType);
            }

            //诊断规则名称
            String detectRulesName = item.getOrDefault(ExportRuleVo.DETECT_RULE_NAME, "").toString();
            if (StringUtils.isEmpty(detectRulesName)) {
                log.info("无诊断规则名称");
                continue;
            }

            //分析字符串
            String anas = item.getOrDefault(ExportRuleVo.ANAS, "").toString();

            //图片名称
            String imageName = item.getOrDefault(ExportRuleVo.IMAGE_NAME, "").toString();
            if (StringUtils.isEmpty(imageName)) {
                log.info("无图片名称");
                continue;
            }

            //诊断判据
            String desOfJudgement = item.getOrDefault(ExportRuleVo.DES, "").toString();

            //图片的路径
            String imgPath = initialPath + StringUtils.SLASH + imageName;
            //新名字
            String newName;
            //封装诊断规则
            DetectRules detectRules = new DetectRules();
            detectRules.setDetectRulesCode(detectRulesCode);
            detectRules.setDetectRulesName(detectRulesName);
            if (StringUtils.isNotEmpty(anas)) {
                detectRules.setAnas(anas);
            }
            detectRules.setBelongType(belongType);
            detectRules.setEquTypeName(equTypeName);
            if (StringUtils.isNotEmpty(desOfJudgement)) {
                detectRules.setDesOfJudgement(desOfJudgement);
            }
            Long userId = SysUtils.getCurrentUserId();
            Date now = new Date();
            detectRules.setImagePath(bucketName);
            detectRules.setCreateTime(now);
            detectRules.setCreator(userId);
            detectRules.setUpdater(userId);
            detectRules.setUpdateTime(now);
            //判断该条诊断规则是否存在于数据库
          /*  lambdaQueryRule.clear();
            lambdaQueryRule.eq(DetectRules::getBelongType, belongType)
                    .eq(DetectRules::getDetectRulesName, detectRulesName)
                    .eq(DetectRules::getEquTypeName, equTypeName);*/

//            DetectRules detectRules = detectRulesService.getOne(lambdaQueryRule, false);
            DetectRules detectRules1 = detectRulesService.getDetectRulesById(dr.getDetectRulesId());


            //挂起查询结果
//            boolean status = StringUtils.isNotNull(detectRules);
            boolean status = StringUtils.isNotNull(detectRules1);

            //更新,进入if就说明不是null
            if (status) {

                detectFormulaMapper.deleteByFormulaId(detectRules.getDetectRulesId());
                log.info("删除对应诊断公式成功");
                //删除原有图片
                //minioSrv.removeObject(detectRulesPo.getBucketName(), detectRulesPo.getImageName());
                //log.info("删除原有图片成功");
                //处理图片名称
                newName = ruleFolder + StringUtils.SLASH + detectRules.getDetectRulesId() + StringUtils.UNDERLINE + belongType + StringUtils.DOT + FileUtil.extName(imgPath);
                detectRules.setImageName(newName);
                //更新
                detectRules.setDetectRulesId(detectRules.getDetectRulesId());
                //查询设备类型id
                equType = selectOrUpdateEquType(equTypeName, belongType);
                detectRules.setEquTypeId(equType.getEquTypeId());
                detectRulesService.updateDetectRules(detectRules);
                log.info("修改第{}行诊断规则成功", index);
            } else {
                //添加
                equType = selectOrUpdateEquType(equTypeName, belongType);
                detectRules.setEquTypeId(equType.getEquTypeId());
                //处理图片名称
                detectRules.setDetectRulesId(IdUtil.getSnowflake().nextId());
                newName = ruleFolder + StringUtils.SLASH + detectRules.getDetectRulesId() + StringUtils.UNDERLINE + belongType + StringUtils.DOT + FileUtil.extName(imgPath);
                detectRules.setImageName(newName);
                detectRulesService.addDetectRules(detectRules);
                log.info("保存第{}行诊断规则成功", index);
            }

            //上传图片
            minioService.putFileBytes(bucketName, FileUtil.readBytes(imgPath), newName, ImageEnum.CONTENT_TYPE);
            //生成缩略图
            String imgName = rulesReduceFolder + StringUtils.SLASH + FileUtil.getName(newName);
            minioService.saveThumbnail(bucketName, imgName, FileUtil.readBytes(imgPath));

            //获取诊断规则内容
            String content = item.getOrDefault(ExportRuleVo.RULE_CONTENT, "").toString();
            if (!StringUtils.isEmpty(content)) {
                JSONArray arr = JSON.parseArray(content);
                if (StringUtils.isNotEmpty(arr)) {
                    for (int i = 0; i < arr.size(); i++) {
                        DetectFormula detectFormula = new DetectFormula();
                        if (status) {
                            detectFormula.setDetectRulesId(detectRules.getDetectRulesId());
                        } else {
                            detectFormula.setDetectRulesId(detectRules.getDetectRulesId());
                        }
                        now = new Date();
                        JSONObject oneCount = arr.getJSONObject(i);
                        detectFormula.setFaultDegreeName(oneCount.getString(ExportRuleVo.E_FAULT_DEGREE));
                        detectFormula.setFormulaDetail(oneCount.getString(ExportRuleVo.E_DETECT_FORMULA));
                        detectFormula.setFormulaDescribe(oneCount.getString(ExportRuleVo.E_DES_OF_FORMULA));
                        detectFormula.setJudgementResult(oneCount.getString(DetectFormula.COL_JUDGEMENT_RESULT));
                        detectFormula.setFaultDescribe(oneCount.getString(ExportRuleVo.E_TERMAL_IMAGE));
                        detectFormula.setHandleSuggestion(oneCount.getString(ExportRuleVo.E_HANDING_SUGGESTION));
                        detectFormula.setCreateTime(now);
                        detectFormula.setUpdateTime(now);
                        detectFormula.setCreator(userId);
                        detectFormula.setUpdater(userId);

//                        detectFormula.setBelongType(belongType);
                        if (StringUtils.isEmpty(FaultDegreeEnum.getDegreeVal(oneCount.getString(ExportRuleVo.E_FAULT_DEGREE)))) {
                            detectFormula.setFaultDegreeId(99L);
                        } else {
                            detectFormula.setFaultDegreeCode(FaultDegreeEnum.getDegreeVal(oneCount.getString(ExportRuleVo.E_FAULT_DEGREE)));
                        }
                        formulaList.add(detectFormula);
                    }
                }
            }
            index++;
        }
        //保存
        try {
            detectFormulaService.saveBatch(formulaList, 1000);


//            detectFormulaService.addDetectFormula();


            log.info("保存第{}行诊断公式成功", index);
        } catch (Exception e) {
            log.error("保存第{}行诊断公式失败", index);
        }
    }

    /**
     * 导入压缩包时使用的
     *
     * @param equTypeName
     * @param belongType
     * @return
     */
    private EquType selectOrUpdateEquType(String equTypeName, String belongType) {


        LambdaQueryWrapper<EquType> lambdaQueryEquType = Wrappers.lambdaQuery(EquType.class);
        lambdaQueryEquType.eq(EquType::getEquTypeName, equTypeName).eq(EquType::getBelongType, belongType);

        EquType eq = new EquType();

//        EquType equ = equTypeService.getOne(lambdaQueryEquType, false);


        EquType equ = equTypeService.getByEquTypeId(eq.getEquTypeId());


        if (StringUtils.isNotNull(equ)) {
            return equ;
        } else {
            Date now = new Date();
            Long userId = SysUtils.getCurrentUserId();
            EquType equType = new EquType();
            equType.setEquTypeName(equTypeName);
            equType.setBelongType(belongType);
            equType.setCreator(userId);
            equType.setUpdater(userId);
            equType.setCreateTime(now);
            equType.setUpdateTime(now);
            equTypeService.addEquType(equType);
            return equType;
        }
    }

}
