package com.tianbo.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/8
 */
@Data
@ApiModel("搜索结果树")
public class SearchVo {

    @ApiModelProperty("输变配")
    private String belongType;

    @ApiModelProperty("设备类型名称")
    private String equTypeName;

}