package com.tianbo.data.service.ex;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/30
 */
public class DetectRulesNotFountException extends ServiceException{
    public DetectRulesNotFountException() {
        super();
    }

    public DetectRulesNotFountException(String message) {
        super(message);
    }

    public DetectRulesNotFountException(String message, Throwable cause) {
        super(message, cause);
    }

    public DetectRulesNotFountException(Throwable cause) {
        super(cause);
    }

    protected DetectRulesNotFountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
