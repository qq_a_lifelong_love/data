package com.tianbo.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/1
 */
@Data
@ApiModel(value = "诊断规则的vo类")
public class RuleTreeVo {

    @ApiModelProperty("父id")
    private Long parentId = 0L;

    @ApiModelProperty("设备类型或诊断规则的id")
    private Long id;

    @ApiModelProperty("设备类型或诊断规则的名称")
    private String label;

    @ApiModelProperty("封装子节点")
    private List<RuleTreeVo> children = new ArrayList<>();

}
