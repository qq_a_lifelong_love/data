package com.tianbo.data.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.tianbo.data.service.OssService;
import com.tianbo.data.utils.ConstantPropertiesUtils;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.UUID;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/31
 */
@Service
public class OssServiceImpl implements OssService {
    @Override
    public String uploadFileImage(MultipartFile file) {
        //用工具类获取值
        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String endpoint = ConstantPropertiesUtils.END_POINT;
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = ConstantPropertiesUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantPropertiesUtils.ACCESS_KEY_SECRET;
        // 填写Bucket名称，例如examplebucket。
        String bucketName = ConstantPropertiesUtils.BUCKET_NAME;


        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
//        String objectName = "exampledir/exampleobject.txt";
        String objectName = file.getOriginalFilename();

//        1   在文件名称里面添加随机唯一的值 tffgrd.01.jpg ---uuid生成的随机字符串中带有-，可以用replaceAll()方法进行全部替换
//        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
//        objectName = uuid + objectName;

//        2   把文件按照日期进行分类   2022/08/27/01.jpg
//        String datePath = new DateTimeLiteralExpression.DateTime().toString("yyyy/MM/dd");
//        objectName = datePath + "/" + objectName;

        // 填写本地文件的完整路径，例如D:\\localpath\\examplefile.txt。
        // 如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
//        String filePath = "D:\\localpath\\examplefile.txt";


        //上传文件流
        try {
            // 创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
//            InputStream inputStream = new FileInputStream(filePath);
//            获取上传文件输入流
            InputStream inputStream = file.getInputStream();
            // 创建PutObject请求。
            ossClient.putObject(bucketName, objectName, inputStream);
//            需要把上传到阿里云oss路径手动拼接出来
//            https://sjh-first.oss-cn-hangzhou.aliyuncs.com/6.jpg
            String url = "https://" + bucketName + "." + endpoint + "/" + objectName;

            return url;

        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (Exception ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());

        }

       /* finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
            return null;
        }*/
        return null;
    }
}
