package com.tianbo.data.service.ex;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/29
 */
public class DetectFormulaNotFountException extends ServiceException{
    public DetectFormulaNotFountException() {
        super();
    }

    public DetectFormulaNotFountException(String message) {
        super(message);
    }

    public DetectFormulaNotFountException(String message, Throwable cause) {
        super(message, cause);
    }

    public DetectFormulaNotFountException(Throwable cause) {
        super(cause);
    }

    protected DetectFormulaNotFountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
