package com.tianbo.data.service.impl;

import cn.hutool.core.util.StrUtil;
import com.tianbo.data.mapper.DetectRulesMapper;
import com.tianbo.data.service.RpcRuleService;
import com.tianbo.data.utils.RedisUtils;
import com.tianbo.data.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/15
 */
public class RpcRuleServiceImpl implements RpcRuleService {

    //redis中的key
    static final String REDIS_KEY = "rules_code:";

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private DetectRulesMapper detectRulesMapper;

    //code的前缀
    static final String PREFIX = "R";

    //数字的长度
    static final String EXPR = "%05d";

    @Override
    public String createRuleCode(String belongType){
        //看看库里有没有
        String key = REDIS_KEY + belongType;
        boolean status = redisUtils.hasKey(key);
        Long number;
        if(status){
            //获取数字
            number = Long.parseLong(redisUtils.getCacheObject(key).toString());
            number += 1;
        }else {
            String code = detectRulesMapper.getRuleCode(belongType);
            if (StringUtils.isEmpty(code)){
                number = 1L;
            }else {
                code = StrUtil.removePrefix(code, PREFIX + belongType);
                number = Long.valueOf(code) + 1;
            }
        }
        //将number保存到redis中
        redisUtils.setCacheObject(key, number);
        String str = String.format(EXPR, number);
        return PREFIX + belongType + str;
    }

}
