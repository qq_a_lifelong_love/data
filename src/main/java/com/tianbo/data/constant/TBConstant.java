package com.tianbo.data.constant;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/5
 */
//所有系统公共常量
public interface TBConstant {
    /**
     * 缓存登录用户详细信息
     */
    String LOGIN_INFO_CACHE = "Authorization:login:user-info:";
    /**
     * sa-token 最后活跃时间
     */
//    String LAST_ACTIVITY_CACHE = "Authorization:login:last-activity:";
    /**
     * sa-token 登录 session
     */
//    String LOGIN_SESSION_CACHE = "Authorization:login:session:";
    /**
     * sa-token 登录 token
     */
//    String LOGIN_TOKEN_CACHE = "Authorization:login:token:";

    /**
     * 菜单列表
     */
//    String MENU_LIST_CACHE = ":menu:list";

    /**
     * 角色列表
     */
//    String ROLE_LIST_CACHE = ":role:list";

    /**
     * 按钮列表
     */
//    String BUTTON_LIST_CACHE = ":button:list";

}
