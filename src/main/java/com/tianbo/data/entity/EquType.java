package com.tianbo.data.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/23
 */
@Api(description = "设备类型表")
@EqualsAndHashCode(callSuper = true)
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "tb_equ_type")
public class EquType extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "设备类型id")
//    @TableId(value = "equ_type_id", type = IdType.ID_WORKER_STR)
    @TableId(value = "equ_type_id")
    private Long equTypeId;

    @ApiModelProperty(value = "设备类型编码")
    @TableField(value = "equ_type_code")
    private String equTypeCode;


    @ApiModelProperty(value = "设备类型名称")
    @TableField(value = "equ_type_name")
    private String equTypeName;

    /**
     * 诊断规则ID
     */
/*    @TableField(value = "detect_rules_id")
    private Long detectRulesId;*/
    @ApiModelProperty(value = "设备类型别名")
    @TableField(value = "equ_type_alias")
    private String equTypeAlias;


    @ApiModelProperty(value = "设备所属类型（0-1-2：变电-输电-配电，其它无效）")
    @TableField(value = "belong_type")
    private String belongType;

    @ApiModelProperty(value = "排序")
    @TableField(value = "sorted")
    private Integer sorted;

/*    public static final String COL_EQU_TYPE_ID = "equ_type_id";

    public static final String COL_EQU_TYPE_CODE = "equ_type_code";

    public static final String COL_EQU_TYPE_NAME = "equ_type_name";

    public static final String COL_DETECT_RULES_ID = "detect_rules_id";

    public static final String COL_BELONG_TYPE = "belong_type";

    public static final String COL_SORTED = "sorted";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_CREATOR = "creator";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_UPDATER = "updater";

    public static final String COL_DELETED = "deleted";

    public static final String COL_VERSION = "version";

    public static final String COL_REMARK = "remark";*/


}
