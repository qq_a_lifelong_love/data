package com.tianbo.data.mapper;

import com.tianbo.data.entity.DetectFormula;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/24
 */
@Mapper
public interface DetectFormulaMapper {


    /**
     * 插入诊断公式
     *
     * @param detectFormula 诊断公式对象
     * @return 受影响的行数
     */
    Integer insertDetectFormula(DetectFormula detectFormula);


    /**
     * 删除的话就是需要把表中的deleted字段改成1
     *
     * @param deleted  默认删除的字段
     * @return 受影响的行数
     */
/*
    Integer updateDeleted(Long deleted);
*/

    /**
     * 根据当前id删除当前诊断公式
     *
     * @param formulaId 诊断公式的id
     * @return 受影响的行数
     */
    void deleteByFormulaId(@Param("detectRulesId") Long formulaId);

    /**
     * 根据当前id进行修改当前的诊断公式的信息
     *
     * @param detectFormula 诊断公式的对象
     * @return 受影响的行数
     */
    Integer updateByFormula(DetectFormula detectFormula);


    /**
     * 根据当前诊断公式的id进行查询当前的诊断公式的详细信息
     *
     * @param formulaId 诊断公式的id
     * @return 当前的诊断公式的的详细信息
     */
    DetectFormula findByFormulaId(Long formulaId);

//////////////////////////////////////

    /**
     * 根据规则id查询诊断公式
     *
     * @param detectRulesId 诊断规则的id
     * @return
     */
    List<DetectFormula> findByFormulaByRuleId(Long detectRulesId);


    /**
     * 根据诊断规则id删除诊断公式
     * @param detectRulesId
     */
  /*  void deleteByRuleId(@Param("detectRulesId") Long detectRulesId);

    List<DetectFormula> selectByRuleCode(@Param("ruleCode") String ruleCode);
    //int batchInsert(@Param("list") List<DetectFormulaPo> list);
    */

    /**
     * 批量查询诊断公式信息
     *
     * @param detectRulesIds
     * @return
     */
    List<DetectFormula> selectFormulaDescription(Long detectRulesIds);

    /**
     * 根据诊断规则id恢复
     *
     * @param list
     * @return
     */
//    int recoverByRuleId(@Param("list") List<Long> list, @Param("userId") Long userId,@Param("time") LocalDateTime time);
    int recoverByRuleId(@Param("list") List<Long> list, @Param("time") LocalDateTime time);


    /**
     * 筛选不分页
     * @param detectFormula
     * @return
     */
    /*    List<DetectFormula> list(@Param("detectFormula") DetectFormula detectFormula);*/

}
