package com.tianbo.data.enums;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/14
 */
public enum ImageEnum {

    IR(0), VI(1);

    private int type;

    private ImageEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static boolean exist(int x) {
        for (ImageEnum e : ImageEnum.values()) {
            if (e.getType() == x) {
                return true;
            }
        }
        return false;
    }

    public static final String FILE_EXT = ".jpg";

    public static final String CONTENT_TYPE = "image/jpeg";

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        for (ImageEnum item : ImageEnum.values()) {
            sb.append(item.getType()).append(",");
        }
    }

}
