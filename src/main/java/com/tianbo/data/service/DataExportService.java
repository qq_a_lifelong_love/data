package com.tianbo.data.service;

import com.tianbo.data.vo.RuleScreenVo;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/5
 */
public interface DataExportService {

    /**
     * 根据筛选条件，导出诊断规则成zip形式
     *
     * @param ruleScreenVo 诊断规则导出excel的vo类
     * @return
     */
    String exportRules(RuleScreenVo ruleScreenVo);
}
