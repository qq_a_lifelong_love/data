package com.tianbo.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/2
 */
@Data
@ApiModel("诊断规则导出excel的vo类")
@AllArgsConstructor
@NoArgsConstructor
public class RuleScreenVo {

    @ApiModelProperty("诊断规则的id数组")
    private Long[] idArray;

    @ApiModelProperty("输变配")
    private String belongType;

}
