package com.tianbo.data.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import lombok.*;

import java.io.Serializable;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/23
 */
@Api(description = "诊断规则表")
@EqualsAndHashCode(callSuper = false)
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "tb_detect_rules")
public class DetectRules extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 诊断规则ID
     */
//    @TableId(value = "detect_rules_id", type = IdType.ID_WORKER_STR)
    @TableId(value = "detect_rules_id")
    private Long detectRulesId;

    /**
     * 诊断规则编码
     */
    @TableField(value = "detect_rules_code")
    private String detectRulesCode;

    /**
     * 诊断规则名称
     */
    @TableField(value = "detect_rules_name")
    private String detectRulesName;

    /**
     * 设备类型ID
     */
    @TableField(value = "equ_type_id")
    private Long equTypeId;

    /**
     * 设备类型名称
     */
    @TableField(value = "equ_type_name")
    private String equTypeName;

    /**
     * 分析字符串
     */
    @TableField(value = "anas")
    private String anas;

    /**
     * 图片名称
     */
    @TableField(value = "image_name")
    private String imageName;

    /**
     * 图片存储类型
     */
    private String imageStorageType;

    /**
     * 图片存储桶(minio)存储本地或者阿里云 兼容3种格式
     */
    @TableField(value = "image_path")
    private String imagePath;

    /**
     * 图片存储桶
     */
/*    @TableField(value = "bucket_name")
    private String bucketName;*/

    /**
     * 所属类型（0-1-2：变电-输电 -配电，其它无效）
     */
    @TableField(value = "belong_type")
    private String belongType;

    /**
     * 诊断判据
     */
    @TableField(value = "des_of_judgement")
    private String desOfJudgement;

    /**
     * 排序
     */
    @TableField(value = "sorted")
    private Integer sorted;


    public static final String COL_DETECT_RULES_ID = "detect_rules_id";

    public static final String COL_DETECT_RULES_CODE = "detect_rules_code";

    public static final String COL_DETECT_RULES_NAME = "detect_rules_name";

    public static final String COL_EQU_TYPE_ID = "equ_type_id";

    public static final String COL_EQU_TYPE_NAME = "equ_type_name";

    public static final String COL_ANAS = "anas";

    public static final String COL_IMAGE_NAME = "image_name";

//    public static final String COL_IMAGE_MD5 = "image_md5";
    public static final String COL_IMAGE_STORAGE_TYPE = "image_storage_type";

    public static final String COL_BUCKET_NAME = "bucket_name";

    public static final String COL_BELONG_TYPE = "belong_type";

    public static final String COL_DES_OF_JUDGEMENT = "des_of_judgement";

    public static final String COL_SORTED = "sorted";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_CREATOR = "creator";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_UPDATER = "updater";

    public static final String COL_DELETED = "deleted";

    public static final String COL_VERSION = "version";

    public static final String COL_REMARK = "remark";

}
