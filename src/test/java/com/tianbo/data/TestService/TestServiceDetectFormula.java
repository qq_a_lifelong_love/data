package com.tianbo.data.TestService;

import com.tianbo.data.entity.DetectFormula;
import com.tianbo.data.entity.DetectRules;
import com.tianbo.data.service.DetectFormulaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/29
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TestServiceDetectFormula {

    @Autowired
    private DetectFormulaService detectFormulaService;


    /*增*/
    @Test
    public void addDetectFormula() {
        DetectFormula detectFormula = new DetectFormula();
        DetectRules detectRules = new DetectRules();
//        detectFormula.setDetectRulesId(21l);
        detectFormula.setDetectRulesId(detectRules.getDetectRulesId());
        detectFormula.setFaultDegreeId(21l);
        detectFormula.setFaultDegreeName("缺陷等级名称");
        detectFormula.setFaultDegreeCode("缺陷等级编码");
        detectFormula.setFormulaCode("公式编码");
        detectFormula.setFormulaName("公式名称");
        detectFormula.setFormulaDetail("诊断公式");
        detectFormula.setJudgementResult("诊断结果(故障特征)");
        detectFormula.setFaultDescribe("故障特征文字描述（热像特征）");
        detectFormula.setFormulaDescribe("公式描述");
        detectFormula.setHandleSuggestion("处理建议");
        detectFormula.setSorted(21);
        detectFormula.setRemark("标记测试");
        detectFormulaService.addDetectFormula(detectFormula);
    }

    /*刪*/
    @Test
    public void deleteDetectFormula() {
        detectFormulaService.deleteDetectFormula(11l);
    }

    /*改*/
    @Test
    public void updateDetectFormula() {
        DetectFormula detectFormula = new DetectFormula();
        detectFormula.setFormulaId(19l);
//        detectFormula.setDetectRulesId(21l);
        detectFormula.setFaultDegreeId(21l);
        detectFormula.setFaultDegreeName("缺陷等级名称99");
        detectFormula.setFaultDegreeCode("缺陷等级编码99");
        detectFormula.setFormulaCode("公式编码99");
        detectFormula.setFormulaName("公式名称");
        detectFormula.setFormulaDetail("诊断公式");
        detectFormula.setJudgementResult("诊断结果(故障特征)");
        detectFormula.setFaultDescribe("故障特征文字描述（热像特征）");
        detectFormula.setFormulaDescribe("公式描述");
        detectFormula.setHandleSuggestion("处理建议");
        detectFormula.setSorted(20);
        detectFormula.setRemark("标记测试");
        detectFormulaService.updateDetectFormula(detectFormula);
    }

    /*查*/
    @Test
    public void getDetectFormulaById() {
        DetectFormula detectFormula = detectFormulaService.getDetectFormulaById(19l);
        System.out.println(detectFormula);
    }

    //    根据诊断规则的id（detectRulesId）查询诊断公式的详细信息
    @Test
    public void getDetectFormulaByDetectRulesId() {
        List<DetectFormula> result = detectFormulaService.getDetectFormulaByDetectRulesId(11l);
        result.forEach(System.out::println);
    }


}
























