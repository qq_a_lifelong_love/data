package com.tianbo.data.service.impl;

import com.tianbo.data.service.MinioService;
import com.tianbo.data.utils.MinioUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/5
 */
@Slf4j
@Service
public class MinioServiceImpl implements MinioService {

    @Autowired
    private MinioUtils minioUtils;

    /**
     * 删除文件
     *
     * @param bucketName
     * @param fileName
     * @return
     */
    @Override
    public boolean removeObject(String bucketName, String fileName) {
        return minioUtils.removeObject(bucketName, fileName);
    }


    @Override
    public String putFileBytes(String bucketName, byte[] fileBytes, String objectName, String contentType) {

        return this.putObject(bucketName, new ByteArrayInputStream(fileBytes), objectName, contentType);
    }


    /**
     * 通过InputStream上传对象，远端文件中心中存储的的文件名为上传流文件的md5值，保证远端存储的文件唯一性，业务端使用的使用可以根据md5进行文件的预览url获取或者流获取。
     *
     * @param bucketName  存储桶名称
     * @param stream      要上传的流
     * @param objectName  minio中文件名：取MD5
     * @param contentType 文件类型
     * @return
     */
    @Override
    public String putObject(String bucketName, InputStream stream, String objectName, String contentType) {
        return minioUtils.putObject(bucketName, stream, objectName, contentType);
    }


    /**
     * 生成缩略图并保存
     *
     * @param bucketName
     * @param objectName
     * @param fileBytes
     * @return
     */
    @Override
    public byte[] saveThumbnail(String bucketName, String objectName, byte[] fileBytes) {
        return minioUtils.saveThumbnail(bucketName, objectName, fileBytes);
    }

    /**
     * 生成一个给HTTP GET请求用的presigned URL。
     * 浏览器/移动端的客户端可以用这个URL进行下载，即使其所在的存储桶是私有的。这个presigned URL可以设置一个失效时间，默认值是7天。
     *
     * @param bucketName 存储桶名称
     * @param objectName 存储桶里的对象名称
     * @param expires    失效时间（以秒为单位），默认是7天，不得大于七天
     * @return
     */
    @Override
    public String preSignedGetObject(String bucketName, String objectName, Integer expires) {
        return minioUtils.preSignedGetObject(bucketName, objectName, expires);
    }

    @Override
    public byte[] getFileBytes(String bucketName, String objectName) {
        InputStream object = minioUtils.getObject(bucketName, objectName);
        if (object != null) {
            return getFileByteArr(object);
        } else {
            return new byte[0];
        }

    }
    /**
     * minio获取的inputStream转换成byte[]
     */
    private static byte[] getFileByteArr(InputStream input) {
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int n = 0;
            while (-1 != (n = input.read(buffer))) {
                output.write(buffer, 0, n);
            }
            output.close();
            input.close();
            return output.toByteArray();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    @SneakyThrows
    public List<String> objectList(String bucketName, String folderName) {
        return minioUtils.listFiles(bucketName, folderName);
    }

}
