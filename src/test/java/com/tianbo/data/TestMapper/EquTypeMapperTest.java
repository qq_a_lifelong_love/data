package com.tianbo.data.TestMapper;

import com.tianbo.data.entity.EquType;
import com.tianbo.data.mapper.EquTypeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/25
 */
//@MapperScan("com.tianbo.data.mapper")
@RunWith(SpringRunner.class)
@SpringBootTest
public class EquTypeMapperTest {

    @Autowired
    private EquTypeMapper equTypeMapper;

    //    添加设备类型信息  成功
    @Test
    public void insertEquType() {
        EquType equType = new EquType();
        equType.setEquTypeCode("104");
        equType.setEquTypeName("name4");
        equType.setEquTypeAlias("otherName4");
        equType.setBelongType("9");
        equType.setVersion(0);
        equType.setSorted(0);
        equType.setDeleted(0);
        Integer integer = equTypeMapper.insertEquType(equType);
        System.out.println(integer);
    }

    //    删除设备类型信息  成功
    @Test
    public void deleteEquType() {
        Integer integer = equTypeMapper.deleteByFormulaId(22l);
        System.out.println(integer);
    }

    //    修改设备类型信息 成功
    @Test
    public void updateEquType() {
        EquType equType = new EquType();
        equType.setEquTypeId(22l);
        equType.setRemark("测2");
        equTypeMapper.updateEquType(equType);


    }


    //    查询设备类型信息 成功
    @Test
    public void findByEquTypeId() {
        EquType byEquTypeId = equTypeMapper.findByEquTypeId(35l);
        System.out.println(byEquTypeId);
    }

}
