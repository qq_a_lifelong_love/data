package com.tianbo.data.vo;

import lombok.Data;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/5
 */
@Data
public class ExportRuleVo {

    public static final String BELONG_TYPE = "输变配";
    public static final String EQU_TYPE_NAME = "设备类型";
    public static final String DETECT_RULE_NAME = "设备形状";
    public static final String IS_ONLY_RULE = "是否单独诊断规则(1:是,0否)";
    public static final String ANAS = "分析字符串";
    public static final String IMAGE_NAME = "图片路径";
    public static final String DES = "诊断判据";
    public static final String MSG = "提示诊断规则名称";
    public static final String RULE_CONTENT = "诊断规则内容";

    public static final String DETECT_RULE_CODE = "编码";

    /**
     * 输变配
     */
    private String belongType;
    public static final String F_BELONG_TYPE = "belongType";

    /**
     * 设备类型名称
     */
    private String equTypeName;
    public static final String F_EQU_TYPE_NAME = "equTypeName";

    /**
     * 诊断规则名称
     */
    private String detectRulesName;
    public static final String F_DETECT_RULE_NAME = "detectRulesName";

    /**
     * 是否单独诊断规则(1:是,0否)
     */
    private Integer isOnlyRule = 0;
    public static final String F_IS_ONLY_RULE = "isOnlyRule";

    private String msg = "";
    public static final String F_MSG = "msg";



    /**
     * 分析字符串
     */
    private String anas;
    public static final String F_ANAS = "anas";

    /**
     * 诊断判据
     */
    private String desOfJudgement;
    public static final String F_DES = "desOfJudgement";

    /**
     * 诊断公式
     */
    private String formulaList;
    public static final String F_RULE_CONTENT = "formulaList";

    /**
     * 图片桶
     */
    private String ImagePath;

    /**
     *图片名称
     */
    private String imageName;
    public static final String F_IMAGE_NAME = "imageName";

    /**
     * 诊断规则id
     */
    private Long detectRulesId;

    /**
     * 诊断规则编码
     */
    private String detectRulesCode;
    public static final String F_DETECT_RULE_CODE = "detectRulesCode";


    /* --- 老数据 --- */

    /**
     * 缺陷等级,对应fault_degree_name
     */
    public static final String E_FAULT_DEGREE = "fault_degree";

    /**
     * 公式详细信息，对应FormulaDetail
     */
    public static final String E_DETECT_FORMULA = "detect_formula";

    /**
     * 公式描述，FormulaDescribe
     */
    public static final String E_DES_OF_FORMULA = "des_of_formula";


    /**
     * 故障特征，对应FaultDescribe
     */
    public static final String E_TERMAL_IMAGE = "thermal_image";

    /**
     * 处理建议，对应HandleSuggestion
     */
    public static final String E_HANDING_SUGGESTION = "handing_suggestion";

}
