package com.tianbo.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tianbo.data.entity.EquType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/25
 */
@Mapper
public interface EquTypeMapper {

    /**
     *添加设备类型
     *
     * @param equType 设备类型的数据
     * @return 受影响的行数
     */
    Integer insertEquType(EquType equType);

    /**
     * 删除的话就是需要把表中的deleted字段改成1
     *
     * @param equTypeId 设备类型的id
     * @return 受影响的行数
     */
    Integer deleteByFormulaId(Long equTypeId);
    /**
     * 根据设备类型的id删除当前设备类型
     *
     * @param equTypeId 设备类型的id
     * @return 返回瘦影响的行数
     */
/*
    Integer deleteEquType(Long equTypeId);
*/

    /**
     * 修改设备类型
     * @param equType 设备类型对象
     * @return 受影响的行数
     */
    Integer updateEquType(EquType equType);


    /**
     * 根据设备类型的id查询当前设备的详细信息
     *
     * @param equTypeId 设备类型id
     * @return 设备类型对象的具体信息
     */
    EquType findByEquTypeId(Long equTypeId);

    /**
     * 根据设备类型的id查询当前设备的详细信息
     *
     * @param equTypeId 设备类型id
     * @return 设备类型对象的具体信息
     */
    EquType getOne2(Long equTypeId);

}
