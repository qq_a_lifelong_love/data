package com.tianbo.data.controller;

import com.tianbo.data.Results.Result;
import com.tianbo.data.service.DataImportService;
import com.tianbo.data.service.MinioService;
import com.tianbo.data.utils.StringUtils;
import com.tianbo.data.utils.SysUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/9
 */

@Slf4j
@RestController
@RequestMapping("/dataImport")
@Api(description = "分片导入接口")
public class DataImportController {


    @Autowired
    private MinioService minioService;

    @Value("${Custom-settings.chunkFolder}")
    private String chunkFolder;

    @Value("${tianbo.minio.buckets}")
    private String bucketName;


    @Autowired
    private DataImportService dataImportService;

    @PostMapping("/upload")
    @Transactional(rollbackFor = Exception.class)
    @ApiOperation("分片上传")
    public Result uploadChunk(
            @ApiParam(name = "file", value = "文件", required = true)
            @RequestParam(value = "file", required = true) MultipartFile file,


    ) {
        /*String md5 = (StringUtils.isNotEmpty(identifier) ? identifier : fileMd5);
        if (StringUtils.isEmptyOrNull(md5)) {
            return Result.fail("md5为空");
        }

        String chunks = (StringUtils.isNotEmpty(chunkNumber) ? chunkNumber : chunk);
        if (StringUtils.isEmptyOrNull(chunks)) {
            return Result.fail("分片值为空");
        }

        log.info("uploadChunk: {},  md5: {} ", chunks, md5);*/
        try {
            /* -----------上传到minio------------ */
            //拼接minio中的保存路径
            String minioPath = chunkFolder + StringUtils.SLASH + md5 + StringUtils.SLASH + chunks;
            //上传
            minioService.putFileBytes(bucketName, file.getBytes(), minioPath, file.getContentType());
            log.info("上传一个名字叫做{}的分片文件成功, {}", file.getOriginalFilename(), chunks);
            return Result.ok(md5);
        } catch (Exception e) {
            log.error("分片上传报错", e);
            return Result.fail(md5);
        }
    }

   /* @ApiOperation("合并分片")
    @Transactional(rollbackFor = Exception.class)
    @GetMapping("/mergeChunks")
    public Result mergeChunks(
            @ApiParam(name = "fileMd5", value = "md5值")
            @RequestParam(value = "fileMd5", required = false)
                    String fileMd5,
            @ApiParam(name = "dataType", value = "业务类型(如1:备份包上传 2:图片包上传 10:诊断规则包上传...)", required = true)
            @RequestParam(value = "dataType", required = true) String dataType,
            //前端的
            @ApiParam(name = "identifier", value = "md5值")
            @RequestParam(value = "identifier", required = false)
                    String identifier,
            //结构校正
            @ApiParam(name = "province", value = "网省名称", required = false)
            @RequestParam(value = "province", required = false, defaultValue = "") String province,

            @ApiParam(name = "city", value = "地市名称", required = false)
            @RequestParam(value = "city", required = false, defaultValue = "") String city,

            @ApiParam(name = "area", value = "区域名称", required = false)
            @RequestParam(value = "area", required = false, defaultValue = "") String area,

            @ApiParam(name = "stationLine", value = "变电站或线路名称", required = false)
            @RequestParam(value = "stationLine", required = false, defaultValue = "") String stationLine,

            @ApiParam(name = "belongType", value = "输变配", required = false)
            @RequestParam(value = "belongType", required = false, defaultValue = "") String belongType
    ) {
        String md5 = (StringUtils.isNotEmpty(identifier) ? identifier : fileMd5);
//        System.out.println(md5 + ", " + SysUtils.getCurrentUserId());
        Result result = dataImportService.mergeChunks(md5, dataType, province.trim(), city.trim(), area.trim(), stationLine.trim(), belongType);
        return result;
    }*/

}
