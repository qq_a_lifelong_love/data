package com.tianbo.data.service.impl;

import com.tianbo.data.entity.DetectFormula;
import com.tianbo.data.entity.DetectRules;
import com.tianbo.data.entity.EquType;
import com.tianbo.data.mapper.DetectFormulaMapper;
import com.tianbo.data.service.DetectFormulaService;
import com.tianbo.data.service.DetectRulesService;
import com.tianbo.data.service.ex.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/8/29
 */
@Service
public class DetectFormulaServiceImpl implements DetectFormulaService {

    @Autowired
    private DetectFormulaMapper detectFormulaMapper;

    @Autowired
    private DetectRulesService detectRulesService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addDetectFormula(DetectFormula detectFormula) {

//        先查看当前有没有诊断公式，
        DetectFormula formula = detectFormulaMapper.findByFormulaId(detectFormula.getFormulaId());
        if (formula != null) {
            throw new InsertException("当前诊断公式已经存在，不用重复的插入");
        }

//        创建诊断公式表需要拿到诊断规则表的 detect_rules_id
//        detectFormula.setDetectRulesId(detectRules.getDetectRulesId());

        //        补全日志信息
        detectFormula.setCreateTime(new Date());
        detectFormula.setUpdateTime(new Date());
        detectFormula.setVersion(0);

        detectFormula.setDeleted(0);
        Integer integer = detectFormulaMapper.insertDetectFormula(detectFormula);
        System.out.println(integer);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteDetectFormula(Long formulaId) {
        //        先查看当前有没有诊断公式，
        DetectFormula formula = detectFormulaMapper.findByFormulaId(formulaId);
        if (formula == null) {
            throw new DetectFormulaNotFountException("当前诊断公式不存在");
        }
        detectFormulaMapper.deleteByFormulaId(formulaId);
    /*    if (integer == 0) {
            throw new DeleteException("删除时产生未知的异常");
        }*/


    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateDetectFormula(DetectFormula detectFormula) {
        DetectFormula formula = detectFormulaMapper.findByFormulaId(detectFormula.getFormulaId());
        if (formula == null) {
            throw new DetectFormulaNotFountException("当前数据不存在这种诊断规则");
        }
        detectFormula.setUpdateTime(new Date());
        Integer integer = detectFormulaMapper.updateByFormula(detectFormula);
        if (integer == 0) {
            throw new UpdateException("修改过程中产生了未知的异常");
        }

    }

    @Override
    public DetectFormula getDetectFormulaById(Long formulaId) {
        DetectFormula formula = detectFormulaMapper.findByFormulaId(formulaId);
        if (formula == null) {
            throw new DetectFormulaNotFountException("未查询到当前的诊断公式");
        }
        return formula;
    }

    //  根据诊断规则的id查询诊断公式的详细信息
    @Override
    public List<DetectFormula> getDetectFormulaByDetectRulesId(Long detectRulesId) {
        List<DetectFormula> list = detectFormulaMapper.findByFormulaByRuleId(detectRulesId);
        return list;
    }

//    批量查询诊断公式信息
    @Override
    public List<DetectFormula> getFormulaDescription(Long detectRulesId) {
        List<DetectFormula> list = detectFormulaMapper.selectFormulaDescription(detectRulesId);
        return list;
    }


    /**
     * 根据诊断规则id恢复
     * @param list
     * @return
     */
    @Override
    public int recoverByRuleId(List<Long> list) {
//        Long userId = SysUtils.getCurrentUserId();
        LocalDateTime time = LocalDateTime.now();
//        return detectFormulaMapper.recoverByRuleId(list, userId, time);
        return detectFormulaMapper.recoverByRuleId(list, time);
    }

    /**
     * 不分页筛选
     * @param detectFormulaPo
     * @return
     */
  /*  @Override
    public List<DetectFormula> list(DetectFormula detectFormula) {
        return detectFormulaMapper.list(detectFormula);
    }*/
}
