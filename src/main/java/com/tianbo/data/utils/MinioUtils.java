package com.tianbo.data.utils;

import cn.hutool.core.img.Img;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.MD5;
import io.minio.*;
import io.minio.http.Method;
import io.minio.messages.Bucket;
import io.minio.messages.DeleteError;
import io.minio.messages.DeleteObject;
import io.minio.messages.Item;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/5
 */
@Slf4j
@Component
public class MinioUtils {




    @Autowired
    private MinioClient minioClient;

    /**
     * 默认url过期时间
     */
    public static final int DEFAULT_EXPIRY_TIME = 7 * 24 * 3600;

    /**
     * 默认最大文件上传为500M
     */
    public static final int MAX_UPLOAD_FILE_SIZE = 1024 * 1024 * 500;

    /**
     * 检查存储桶是否存在
     *
     * @param bucketName 存储桶名称
     * @return
     */
    @SneakyThrows
    public boolean bucketExists(String bucketName) {
        return minioClient.bucketExists(BucketExistsArgs.builder()
                .bucket(bucketName)
                .build()
        );
    }

    /**
     * 创建存储桶
     *
     * @param bucketName 存储桶名称
     */
    @SneakyThrows
    public void makeBucket(String bucketName) {
        if (!bucketExists(bucketName)) {
            MakeBucketArgs.builder().bucket(bucketName).build();
        }
    }

    /**
     * 列出所有存储桶
     *
     * @return
     */
    @SneakyThrows
    public List<Bucket> listBuckets() {
        return minioClient.listBuckets();
    }

    /**
     * 列出所有存储桶名称
     *
     * @return
     */
    @SneakyThrows
    public List<String> listBucketNames() {
        List<Bucket> bucketList = listBuckets();
        return CollectionUtils.isNotEmpty(bucketList) ?
                bucketList.stream().map(Bucket::name).collect(Collectors.toList()) : new ArrayList<>();
    }

    /**
     * 删除存储桶
     *
     * @param bucketName 存储桶名称
     * @return
     */
    @SneakyThrows
    public boolean removeBucket(String bucketName) {
        boolean flag = bucketExists(bucketName);
        if (flag) {
            Iterable<Result<Item>> myObjects = listObjects(bucketName);
            for (Result<Item> result : myObjects) {
                Item item = result.get();
                // 有对象文件，则删除失败
                if (item.size() > 0) {
                    return false;
                }
            }
            // 删除存储桶，注意，只有存储桶为空时才能删除成功。
            minioClient.removeBucket(RemoveBucketArgs.builder().bucket(bucketName).build());
            flag = bucketExists(bucketName);
            return !flag;
        }
        return false;
    }

    /**
     * 列出存储桶中的所有对象名称
     *
     * @param bucketName 存储桶名称
     * @return
     */
    @SneakyThrows
    public List<String> listObjectNames(String bucketName) {
        List<String> listObjectNames = new ArrayList<>();
        boolean flag = bucketExists(bucketName);
        if (flag) {
            Iterable<Result<Item>> myObjects = listObjects(bucketName);
            for (Result<Item> result : myObjects) {
                Item item = result.get();
                listObjectNames.add(item.objectName());
            }
        }
        return listObjectNames;
    }

    /**
     * 列出存储桶中的所有对象
     *
     * @param bucketName 存储桶名称
     * @return
     */
    @SneakyThrows
    public Iterable<Result<Item>> listObjects(String bucketName) {
        boolean flag = bucketExists(bucketName);
        if (flag) {
            return minioClient.listObjects(ListObjectsArgs.builder().bucket(bucketName).build());
        }
        return null;
    }

    /**
     * 获取文件md5
     *
     * @param stream
     * @return
     */
    public String getFileMd5(InputStream stream) {
        return MD5.create().digestHex(stream);
    }

    /**
     * 获取文件md5
     *
     * @param multipartFile
     * @return
     */
    @SneakyThrows
    public String getFileMd5(MultipartFile multipartFile) {
        return this.getFileMd5(multipartFile.getInputStream());
    }

    /**
     * 文件上传
     *
     * @param bucketName
     * @param multipartFile
     */
    @SneakyThrows
    public String putObject(String bucketName, MultipartFile multipartFile) {
        Assert.isTrue(multipartFile.getSize() <= MAX_UPLOAD_FILE_SIZE, "com.tianbo.minio.upload.file.is.too.big");
        Assert.isTrue(bucketExists(bucketName), "com.tianbo.minio.bucket.is.not.exist");
        String objectName = this.getFileMd5(multipartFile);
        return this.putObject(bucketName, multipartFile.getInputStream(), objectName, multipartFile.getContentType());
    }

    /**
     * 通过InputStream上传对象，远端文件中心中存储的的文件名为上传流文件的md5值，保证远端存储的文件唯一性，业务端使用的使用可以根据md5进行文件的预览url获取或者流获取。
     *
     * @param bucketName  存储桶名称
     * @param stream      要上传的流
     * @param objectName  minio中文件名：取MD5
     * @param contentType 文件类型
     * @return
     */
    @SneakyThrows
    public String putObject(String bucketName, InputStream stream, String objectName, String contentType) {
        Assert.isTrue(bucketExists(bucketName), "com.tianbo.minio.bucket.is.not.exist");
        Assert.isTrue(StrUtil.isNotBlank(objectName), "com.tianbo.minio.objectName.is.not.exist");
        ObjectWriteResponse objectWriteResponse = minioClient.putObject(
                PutObjectArgs.builder()
                        .bucket(bucketName)
                        .object(objectName)
                        .contentType(contentType)
                        .stream(stream, stream.available(), -1)
                        .build()
        );
        stream.close();
        return objectWriteResponse.object();
    }

    /**
     * 以流的形式获取一个文件对象
     *
     * @param bucketName 存储桶名称
     * @param objectName 存储桶里的对象名称
     * @return
     */
    @SneakyThrows
    public InputStream getObject(String bucketName, String objectName) {
        boolean flag = bucketExists(bucketName);
        if (flag) {
            StatObjectResponse statObjectResponse = statObject(bucketName, objectName);
            if (statObjectResponse != null && statObjectResponse.size() > 0) {
                return minioClient.getObject(GetObjectArgs.builder()
                        .bucket(bucketName)
                        .object(objectName)
                        .build()
                );
            }
        }
        return null;
    }

    /**
     * 以流的形式获取一个文件对象（断点下载）
     *
     * @param bucketName 存储桶名称
     * @param objectName 存储桶里的对象名称
     * @param offset     起始字节的位置
     * @param length     要读取的长度 (可选，如果无值则代表读到文件结尾)
     * @return
     */
    @SneakyThrows
    public InputStream getObject(String bucketName, String objectName, long offset, Long length) {
        boolean flag = bucketExists(bucketName);
        if (flag) {
            StatObjectResponse statObjectResponse = statObject(bucketName, objectName);
            if (statObjectResponse != null && statObjectResponse.size() > 0) {
                return minioClient.getObject(GetObjectArgs.builder()
                        .bucket(bucketName)
                        .object(objectName)
                        .offset(offset)
                        .length(length)
                        .build()
                );
            }
        }
        return null;
    }

    /**
     * 删除一个对象
     *
     * @param bucketName 存储桶名称
     * @param objectName 存储桶里的对象名称
     */
    @SneakyThrows
    public boolean removeObject(String bucketName, String objectName) {
        boolean flag = bucketExists(bucketName);
        if (flag) {
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
            return true;
        }
        return false;
    }

    /**
     * 删除指定桶的多个文件对象,返回删除错误的对象列表，全部删除成功，返回空列表
     *
     * @param bucketName  存储桶名称
     * @param objectNames 含有要删除的多个object名称的迭代器对象
     * @return
     */
    @SneakyThrows
    public List<String> removeObject(String bucketName, List<String> objectNames) {
        Assert.isTrue(CollectionUtils.isNotEmpty(objectNames), "com.tianbo.minio.delete.object.name.can.not.empty");
        List<String> deleteErrorNames = new ArrayList<>();
        boolean flag = bucketExists(bucketName);
        if (flag) {
            List<DeleteObject> objects = objectNames.stream().map(DeleteObject::new).collect(Collectors.toList());
            Iterable<Result<DeleteError>> results = minioClient
                    .removeObjects(RemoveObjectsArgs.builder().bucket(bucketName).objects(objects).build());
            for (Result<DeleteError> result : results) {
                DeleteError error = result.get();
                deleteErrorNames.add(error.objectName());
            }
        }
        return deleteErrorNames;
    }

    /**
     * 生成一个给HTTP GET请求用的presigned URL。
     * 浏览器/移动端的客户端可以用这个URL进行下载，即使其所在的存储桶是私有的。这个presigned URL可以设置一个失效时间，默认值是7天。
     *
     * @param bucketName 存储桶名称
     * @param objectName 存储桶里的对象名称
     * @param expires    失效时间（以秒为单位），默认是7天，不得大于七天
     * @return
     */
    @SneakyThrows
    public String preSignedGetObject(String bucketName, String objectName, Integer expires) {
        // todo 需做安全性完善
        boolean flag = bucketExists(bucketName);
        String url = "";
        if (flag) {
            url = minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                    .method(Method.GET)
                    .bucket(bucketName)
                    .object(objectName)
                    .expiry(Objects.isNull(expires) ? DEFAULT_EXPIRY_TIME : expires)
                    .build()
            );
        }
        return url;
    }

    /**
     * 生成一个给HTTP PUT请求用的presigned URL。
     * 浏览器/移动端的客户端可以用这个URL进行上传，即使其所在的存储桶是私有的。这个presigned URL可以设置一个失效时间，默认值是7天。
     *
     * @param bucketName 存储桶名称
     * @param objectName 存储桶里的对象名称
     * @param expires    失效时间（以秒为单位），默认是7天，不得大于七天
     * @return
     */
    @SneakyThrows
    public String preSignedPutObject(String bucketName, String objectName, Integer expires) {
        boolean flag = bucketExists(bucketName);
        String url = "";
        if (flag) {
            url = minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                    .method(Method.PUT)
                    .bucket(bucketName)
                    .object(objectName)
                    .expiry(Objects.isNull(expires) ? DEFAULT_EXPIRY_TIME : expires)
                    .build()
            );
        }
        return url;
    }

    /**
     * 获取对象的元数据
     *
     * @param bucketName 存储桶名称
     * @param objectName 存储桶里的对象名称
     * @return
     */
    @SneakyThrows
    public StatObjectResponse statObject(String bucketName, String objectName) {
        boolean flag = bucketExists(bucketName);
        if (flag) {
            return minioClient.statObject(StatObjectArgs.builder().bucket(bucketName).object(objectName).build());
        }
        return null;
    }

    /**
     * 文件下载
     *
     * @param bucketName   桶名称
     * @param objectName   桶中文件名
     * @param originalName 下载文件的名称
     * @param request      请求
     * @param response     请求响应
     */
    public void downloadFile(String bucketName,
                             String objectName,
                             String originalName,
                             HttpServletRequest request,
                             HttpServletResponse response) {
        try {
            InputStream file = getObject(bucketName, objectName);
            String fileName = StrUtil.isNotEmpty(originalName) ? originalName : objectName;
            fileName = fileName.replace(" ", "");
            //文件名乱码处理
            String useragent = request.getHeader("USER-AGENT").toLowerCase();
            if (useragent.contains("msie") || useragent.contains("like gecko") || useragent.contains("trident")) {
                fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.displayName());
            } else {
                fileName = new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
            }
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            ServletOutputStream servletOutputStream = response.getOutputStream();
            int len;
            byte[] buffer = new byte[1024];
            while ((len = file.read(buffer)) > 0) {
                servletOutputStream.write(buffer, 0, len);
            }
            servletOutputStream.flush();
            file.close();
            servletOutputStream.close();
        } catch (Exception e) {
            log.error("下载文件:{}异常", objectName, e);
        }
    }

    /**
     * 列出一个桶中的所有文件和目录
     */
    public List<String> listFiles(String bucket, String folderName) throws Exception {
        Iterable<Result<Item>> results = minioClient.listObjects(
                ListObjectsArgs.builder().bucket(bucket).prefix(folderName).recursive(true).build());

        List<String> infos = new ArrayList<>();
        results.forEach(r -> {
            try {
                Item item = r.get();
                infos.add(item.objectName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return infos;
    }

    /**
     * 生成缩略图并保存
     *
     * @param objectName 原始图片名称
     * @param folder     缩略图在minio中的保存文件夹
     * @param bucketName 存储桶名称
     * @param path       作为周转的缩略图的保存路径
     * @return
     */
//    public String saveThumbnail(String objectName, String newName, String folder, String bucketName, String path, Integer width, Integer height) {
//        File originalFile = new File(objectName);
//        //获取原始图片的名称
//        String originalName = FileUtil.getName(objectName);
//        //创建缩略图
//        File thumbnail = new File(path + originalName);
//        FileUtil.touch(thumbnail);
//        ImgUtil.scale(originalFile, thumbnail, width, height, null);
//        //将缩略图保存
//        return putObject(bucketName, FileUtil.getInputStream(thumbnail), folder + "/" + FileUtil.getName(newName), "image/jpeg");
//    }

    /**
     * 生成缩略图并保存
     */
    @SneakyThrows
    public byte[] saveThumbnail(String bucketName, String objectName, byte[] fileBytes) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(fileBytes);
        byte[] bufferedImageBytes = getBufferedImageBytes(inputStream, null);

        putObject(bucketName, new ByteArrayInputStream(bufferedImageBytes), objectName, "image/jpeg");

        inputStream.close();

        return bufferedImageBytes;
    }


    private static byte[] getBufferedImageBytes(InputStream in, Float quality) {
        //这个代码对jdk版本有依赖，容易出bug
        BufferedImage originImage = null;
        try {
            originImage = ImageIO.read(in);
            int width = originImage.getWidth();
            int height = originImage.getHeight();
            //类型需要仔细分析
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
            image.getGraphics().drawImage(originImage, 0, 0, null);
            if (quality == null) {
                quality = 0.8f;
            }
            byte[] imageBytes = getBufferedImageBytes(image, quality);
            in.close();
            return imageBytes;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    private static byte[] getBufferedImageBytes(BufferedImage source, float quality) {
        if (null == source) {
            return null;
        }
        Image image = Img.from(source).setQuality(quality).getImg();
        return ImgUtil.toBytes(image,null);
    }



}
